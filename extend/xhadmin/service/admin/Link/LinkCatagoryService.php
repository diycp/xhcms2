<?php 
/**
 *友情链接分类
*/

namespace xhadmin\service\admin\Link;

use xhadmin\CommonService;
use xhadmin\db\Link\LinkCatagory;

class LinkCatagoryService extends CommonService {


	/*
 	* @Description  友情链接分类列表数据
 	* @param (输入参数：)  {array}        where 查询条件
 	* @param (输入参数：)  {int}          limit 分页参数
 	* @param (输入参数：)  {String}       field 查询字段
 	* @param (输入参数：)  {String}       orderby 排序字段
 	* @return (返回参数：) {array}        分页数据集
 	*/
	public static function pageList($where=[],$limit,$field='*',$orderby=''){
		try{
			$list = LinkCatagory::loadList($where,$limit,$field,$orderby);
			$count = LinkCatagory::countList($where);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return ['list'=>$list,'count'=>$count];
	}


	/*
 	* @Description  添加
 	* @param (输入参数：)  {array}        data 原始数据
 	* @return (返回参数：) {bool}        
 	*/
	public static function add($data){
		try{

			//数据验证
			$rule = [
				'class_name'=>['require'],
			];
			//数据错误提示
			$msg = [
				'class_name.require'=>'分类名称不能为空',
			];
			self::validate($rule,$data,$msg);

			$res = LinkCatagory::createData($data);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}


	/*
 	* @Description  修改
 	* @param (输入参数：)  {array}        data 原始数据
 	* @return (返回参数：) {bool}        
 	*/
	public static function update($data){
		try{

			//数据验证
			$rule = [
				'class_name'=>['require'],
			];
			$msg = [
				'class_name.require'=>'分类名称不能为空',
			];
			self::validate($rule,$data,$msg);

			$res = LinkCatagory::edit($data);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}


	/*
 	* @Description  删除
 	* @param (输入参数：)  {array}        where 删除条件
 	* @return (返回参数：) {bool}        
 	*/
	public static function delete($where){
		try{
			$res = LinkCatagory::delete($where);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}




}


/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : xhadmin_test

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-10-29 15:16:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cd_access`
-- ----------------------------
DROP TABLE IF EXISTS `cd_access`;
CREATE TABLE `cd_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分组ID',
  `purviewval` varchar(128) DEFAULT NULL COMMENT '分组对应权限值',
  `group_id` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2054 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_access
-- ----------------------------
INSERT INTO `cd_access` VALUES ('2053', '/admin/Base/password', '3');
INSERT INTO `cd_access` VALUES ('2052', '/admin/Base/password', '3');
INSERT INTO `cd_access` VALUES ('2051', '/admin/Sys', '3');
INSERT INTO `cd_access` VALUES ('2050', '/admin/Cms.Position/delete', '3');
INSERT INTO `cd_access` VALUES ('2049', '/admin/Cms.Position/update', '3');
INSERT INTO `cd_access` VALUES ('2048', '/admin/Cms.Position/add', '3');
INSERT INTO `cd_access` VALUES ('2046', '/admin/Cms.Position', '3');
INSERT INTO `cd_access` VALUES ('2047', '/admin/Cms.Position/index', '3');
INSERT INTO `cd_access` VALUES ('2045', '/admin/Cms.Frament/delete', '3');
INSERT INTO `cd_access` VALUES ('2044', '/admin/Cms.Frament/update', '3');
INSERT INTO `cd_access` VALUES ('2043', '/admin/Cms.Frament/add', '3');
INSERT INTO `cd_access` VALUES ('2042', '/admin/Cms.Frament/index', '3');
INSERT INTO `cd_access` VALUES ('2041', '/admin/Cms.Frament', '3');
INSERT INTO `cd_access` VALUES ('2040', '/admin/Cms.Content/setStatus', '3');
INSERT INTO `cd_access` VALUES ('2039', '/admin/Cms.Content/delPosition', '3');
INSERT INTO `cd_access` VALUES ('2037', '/admin/Cms.Content/move', '3');
INSERT INTO `cd_access` VALUES ('2038', '/admin/Cms.Content/setPosition', '3');
INSERT INTO `cd_access` VALUES ('2036', '/admin/Cms.Content/updateSort', '3');
INSERT INTO `cd_access` VALUES ('2035', '/admin/Cms.Content/update', '3');
INSERT INTO `cd_access` VALUES ('2033', '/admin/Cms.Content/add', '3');
INSERT INTO `cd_access` VALUES ('2034', '/admin/Cms.Content/delete', '3');
INSERT INTO `cd_access` VALUES ('2032', '/admin/Cms.Content/index', '3');
INSERT INTO `cd_access` VALUES ('2031', '/admin/Cms.Content', '3');
INSERT INTO `cd_access` VALUES ('2030', '/admin/Cms.Catagory/setSort', '3');
INSERT INTO `cd_access` VALUES ('2029', '/admin/Cms.Catagory/updateSort', '3');
INSERT INTO `cd_access` VALUES ('2028', '/admin/Cms.Catagory/delete', '3');
INSERT INTO `cd_access` VALUES ('2027', '/admin/Cms.Catagory/update', '3');
INSERT INTO `cd_access` VALUES ('2026', '/admin/Cms.Catagory/add', '3');
INSERT INTO `cd_access` VALUES ('2025', '/admin/Cms.Catagory/index', '3');
INSERT INTO `cd_access` VALUES ('2024', '/admin/Cms.Catagory', '3');
INSERT INTO `cd_access` VALUES ('2023', '/admin/Cms', '3');
INSERT INTO `cd_access` VALUES ('2022', '/admin/Member/batchUpdate', '3');
INSERT INTO `cd_access` VALUES ('2021', '/admin/Member/updatePassword', '3');
INSERT INTO `cd_access` VALUES ('2020', '/admin/Member/start', '3');
INSERT INTO `cd_access` VALUES ('2019', '/admin/Member/forbidden', '3');
INSERT INTO `cd_access` VALUES ('2018', '/admin/Member/delete', '3');
INSERT INTO `cd_access` VALUES ('2017', '/admin/Member/backRecharge', '3');
INSERT INTO `cd_access` VALUES ('2016', '/admin/Member/recharge', '3');
INSERT INTO `cd_access` VALUES ('2015', '/admin/Member/add', '3');
INSERT INTO `cd_access` VALUES ('2014', '/admin/Member/viewMember', '3');
INSERT INTO `cd_access` VALUES ('2013', '/admin/Member/index', '3');
INSERT INTO `cd_access` VALUES ('2012', '/admin/Member', '3');

-- ----------------------------
-- Table structure for `cd_catagory`
-- ----------------------------
DROP TABLE IF EXISTS `cd_catagory`;
CREATE TABLE `cd_catagory` (
  `class_id` int(10) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(250) DEFAULT NULL,
  `subtitle` varchar(250) DEFAULT NULL COMMENT '副标题',
  `type` tinyint(4) DEFAULT NULL,
  `list_tpl` varchar(250) DEFAULT NULL,
  `detail_tpl` varchar(250) DEFAULT NULL,
  `pic` varchar(250) DEFAULT NULL,
  `keyword` varchar(250) DEFAULT NULL,
  `description` text,
  `jumpurl` varchar(250) DEFAULT NULL,
  `sortid` int(9) DEFAULT NULL,
  `pid` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `filepath` varchar(255) DEFAULT NULL,
  `filename` varchar(32) DEFAULT NULL COMMENT '生成文件名',
  `module_id` smallint(5) DEFAULT NULL,
  `upload_config_id` int(9) NOT NULL,
  PRIMARY KEY (`class_id`),
  UNIQUE KEY `class_id` (`class_id`),
  KEY `class_name` (`class_name`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_catagory
-- ----------------------------
INSERT INTO `cd_catagory` VALUES ('7', '公司简介', '', '1', 'about', '', '', '', '', '', '1', '0', '1', '/html/gongsijianjie', 'index.html', '39', '0');
INSERT INTO `cd_catagory` VALUES ('8', '产品中心', '', '2', 'pic', 'viewproduct', '', '', '', '', '2', '0', '1', '/html/chanpinzhongxin', 'index.html', '0', '0');
INSERT INTO `cd_catagory` VALUES ('9', '新闻资讯', '', '2', 'list', 'view', '', '', '', '', '9', '0', '1', '/html/xinwenzixun', 'index.html', '0', '0');
INSERT INTO `cd_catagory` VALUES ('10', '人才招聘', '', '1', 'about', '', '', '', '', '', '2', '0', '1', '/html/rencaizhaopin', 'index.html', '0', '0');
INSERT INTO `cd_catagory` VALUES ('11', '在线留言', '', '1', 'gustbook', '', '', '', '', '/about/11', '11', '0', '1', '/html/zaixianliuyan', 'index.html', '0', '0');
INSERT INTO `cd_catagory` VALUES ('12', '联系我们', '', '1', 'about', '', '', '', '', '', '12', '0', '1', '/html/lianxiwomen', 'index.html', '0', '0');
INSERT INTO `cd_catagory` VALUES ('13', 'intel网卡', '', '2', 'pic', 'viewproduct', '', '', '', '', '15', '8', '1', '/html/intelwangka', 'index.html', '23', '0');
INSERT INTO `cd_catagory` VALUES ('14', 'bcm网卡', '', '2', 'pic', 'viewproduct', '', '', '', '', '16', '8', '1', '/html/bcmwangka', 'index.html', '24', '0');
INSERT INTO `cd_catagory` VALUES ('15', '惠普HP网卡系列', '', '2', 'pic', 'viewproduct', '', '', '', '', '14', '8', '1', '/html/huipuHPwangkaxilie', 'index.html', '0', '0');
INSERT INTO `cd_catagory` VALUES ('16', 'IBM网卡', '', '2', 'pic', 'viewproduct', '', '', '', '', '13', '8', '1', '/html/IBMwangka', 'index.html', '0', '0');
INSERT INTO `cd_catagory` VALUES ('17', '光纤模块系列', '', '2', 'pic', 'viewproduct', '', '', '', '', '17', '8', '1', '/html/guangxianmokuaixilie', 'index.html', '0', '0');
INSERT INTO `cd_catagory` VALUES ('19', 'banner', '', '1', '', '', '', '', '', '', '19', '0', '1', '/html/banner', 'index.html', '0', '4');
INSERT INTO `cd_catagory` VALUES ('20', '测试栏目', '', '2', 'pic', 'viewproduct', '', '', '', '', '20', '16', '1', '/html/IBMwangka/ceshilanmu', 'index.html', '0', '0');
INSERT INTO `cd_catagory` VALUES ('21', '在测试', '', '2', 'pic', 'viewproduct', '', '', '', '', '21', '20', '1', '/html/IBMwangka/ceshilanmu/zaiceshi', 'index.html', '0', '0');

-- ----------------------------
-- Table structure for `cd_config`
-- ----------------------------
DROP TABLE IF EXISTS `cd_config`;
CREATE TABLE `cd_config` (
  `name` varchar(50) NOT NULL,
  `data` varchar(250) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_config
-- ----------------------------
INSERT INTO `cd_config` VALUES ('cnzz', '');
INSERT INTO `cd_config` VALUES ('copyright', '寒塘冷月 qq:274363574');
INSERT INTO `cd_config` VALUES ('default_themes', 'index');
INSERT INTO `cd_config` VALUES ('description', '停车管理系统');
INSERT INTO `cd_config` VALUES ('domain', '');
INSERT INTO `cd_config` VALUES ('email_pwd', '');
INSERT INTO `cd_config` VALUES ('email_user', '');
INSERT INTO `cd_config` VALUES ('file_size', '50');
INSERT INTO `cd_config` VALUES ('file_type', 'gif,png,jpg,jpeg,doc,docx,xls,xlsx,csv,pdf,rar,zip,txt,mp4,flv');
INSERT INTO `cd_config` VALUES ('images_size', '10M');
INSERT INTO `cd_config` VALUES ('keyword', '停车场,高铁,飞机场,测试');
INSERT INTO `cd_config` VALUES ('mobil_domain', '');
INSERT INTO `cd_config` VALUES ('mobil_status', '2');
INSERT INTO `cd_config` VALUES ('mobil_themes', '');
INSERT INTO `cd_config` VALUES ('off_msg', '站点维护中');
INSERT INTO `cd_config` VALUES ('port', '');
INSERT INTO `cd_config` VALUES ('site_logo', '/uploads/admin/201910/5db6890644255.jpg');
INSERT INTO `cd_config` VALUES ('site_status', '1');
INSERT INTO `cd_config` VALUES ('site_title', 'xhcms后台系统');
INSERT INTO `cd_config` VALUES ('smtp', '');
INSERT INTO `cd_config` VALUES ('sub_title', '武汉网站建设');
INSERT INTO `cd_config` VALUES ('water_logo', '');
INSERT INTO `cd_config` VALUES ('water_status', '1');

-- ----------------------------
-- Table structure for `cd_content`
-- ----------------------------
DROP TABLE IF EXISTS `cd_content`;
CREATE TABLE `cd_content` (
  `content_id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `class_id` tinyint(4) DEFAULT NULL,
  `pic` varchar(250) DEFAULT NULL,
  `detail` text,
  `status` tinyint(4) DEFAULT NULL,
  `position` varchar(250) DEFAULT NULL,
  `jumpurl` varchar(250) DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `keyword` varchar(250) DEFAULT NULL,
  `description` text,
  `views` varchar(250) DEFAULT '1',
  `sortid` varchar(250) DEFAULT NULL,
  `author` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`content_id`),
  KEY `title` (`title`),
  KEY `class_id` (`class_id`),
  KEY `create_time` (`create_time`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_content
-- ----------------------------
INSERT INTO `cd_content` VALUES ('16', 'Emulex第五代FC HBA实现230万IOPS！', '20', '', '<p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">几年前，每秒输入/输出操作数（IOPS）突破10万次都可以说是惊人的事件；但是就在最近，Storage Switzerland与博科、戴尔、Violin Memory（闪存厂商）和Emulex合作进行了实验室测试，在广泛采用现有组件搭建的简单的存储区域网络（SAN）上， 实现了230万IOPS！<br /><br />测试环境在16Gb光纤通道基础架构上采用了4台Dell PowerEdge R910服务器，每台服务器都配有4个Emulex LightPulse第五代光纤通道（FC）LPe16002B主机总线适配器（HBA）。这些板卡用于把服务器连接到一个博科第五代FC 6520交换机上，然后这台交换机再连接到Violin 6616 Flash Memory SAN阵列。安装整套设备仅需一个标准机架，而且机架空间还有空余。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">利用快闪存储设备（本例中使用的是4个Violin Flash Memory SAN阵列），我们可以获得快得多的IOPS速度，因为快闪存储的速度最高可比传统存储产品快90%。通过功能强大的新型服务器（本例中采用了4台Dell PowerEdge R910），我们能够真正提高处理事务的速度，把阵列充分利用起来。但是数据是如何从服务器传输到存储系统的呢？当然是通过HBA(Host Bus Adapter)和交换机。Storage Switzerland认为，需要一种周到全面的方式才能搭建出真正高性能的SAN，而我们Emulex认为，SAN的性能高低取决于它最薄弱的链路环节。试想，我们总不能把浇花用的水管接到消防栓上还指望用它扑灭熊熊大火吧！</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">大型数据中心SAN的连接介质大都运行在50万IOPS的数据速率上，因此230万的IOPS目前看来似乎难以想象。但我们必须基于整体角度考虑，为什么对于当今的数据要求和应用来说，230万IOPS不是一个高得多余（“为什么我需要那么快的SAN”）的指标。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">原因之一是“虚拟化”。现在的服务器比过去强大了许多，据估计，每台物理服务器上平均用VMware等监管程序运行着25个虚拟机（VM），实际上我们在服务器上通常看到的密度会更高。在这种VM环境下，如果没有足够强大的HBA，就极有可能产生I/O瓶颈。云应用也在推动I/O性能要求提高。随着越来越多的数据中心实施混合云环境，送入SAN的整体数据量会明显增加。最后，大数据解决方案的发展也需要速度更快的存储产品和更高的I/O性能。就是这些原因最终会迫使我们向16GFC基础架构和快闪存储阵列配置迁移。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">但目前许多数据中心还在使用8GFC基础架构（存储系统、交换机以及HBA），而且尚未准备升级到16GFC，它们也可能会选择把钱花在快闪阵列或者服务器侧的快闪缓存上。那么如果基础架构没有改变，升级到16GFC HBA（特别是Emulex第五代产品）有什么好处呢？</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">理由：首先，Emulex第五代光纤通道HBA能够与8GFC环境无缝结合，实现真正的性能提升，还可以把IOPS提高到原来的5倍（超过120万），同时把延时降到只有8GFC的一半。其次，所有第五代FC HBA都支持PCIe 3.0，这直接提升了运行速度，而且PCIe 3.0在所有新服务器上都是标准配置，既然是这样，为什么不把PCIe 3.0的优点充分利用起来呢？第三，因为无需购买16GFC交换机或存储系统，因此这是最简单、投资最少的升级SAN性能的方法。第四，它是面向未来的，而将来则不可避免地要向16GFC基础架构迁移。此外，Emulex第五代FC HBA还100%向后兼容，用户完全不必担心兼容问题。</p>', '1', '', '', '1552822476', '', '', '51', '16', '');
INSERT INTO `cd_content` VALUES ('17', 'Emulex为戴尔PowerEdge提供高性能虚拟化和可扩展性', '9', '', '<p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">Emulex公司日前宣布推出用于Dell PowerEdge机架、刀片和塔式平台的高性能万兆以太网（10GbE）连接产品，即全新OneConnect OCe14000系列万兆以太网融合网络适配器（CNA）。OCe14000系列万兆以太网CNA专为虚拟化、企业和云数据中心而进行了优化，可以提供无状态TCP硬件卸载能力。它通过单一源I/O虚拟化（Single-Root I/O Virtualization，SR-IOV）来提高虚拟化可扩展性，并通过FCoE和iSCSI硬件卸载优化CPU利用率。同时，它可使用戴尔交换独立的网络分区（switch independent network partitioning，NPAR）技术来优化带宽分配，并且通过Emulex叠加网络（Overlay Network，OVN）卸载技术来加速云网络。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">Emulex营销及企业发展高级副总裁Shaun Walsh表示：“Emulex OCe14000 10GbE CNA专门针对戴尔服务器平台进行了优化，以其实现差异化。其设计目标是解决企业、云和虚拟化应用（包括软件定义网络采用的新技术）面临的诸多问题。为戴尔产品配备最新Emulex万兆以太网产品使我们的合作更紧密，从而能够在网络领域内开发出差异化的端到端解决方案。这些新解决方案还获得了用于所有Dell Force10交换机产品的认证，因此用户可以使用戴尔服务器、存储系统和交换机搭建出完整的解决方案。OCe14000 10GbE CNA的推出使戴尔客户有更多Emulex产品可供选择，完善了我们目前的互连产品种类，这些产品中还包括了LightPulse&reg;第五代光纤通道主机总线适配器（HBA）。”</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">Emulex OneConnect 10GbE CNA技术可以实现非常高的虚拟机（VM）密度，通过OVN卸载技术支持安全的混合云，并提供了开放式应用程序接口（API），通过它能够与新一代SDN解决方案进行集成。经过验证，OneConnect 万兆以太网CNA不仅可以用作Dell PowerEdge平台的出厂预装选件，而且还可以用于完善其他戴尔融合式基础架构平台，如支持FCoE的Dell S5000网络交换机，以及Dell Compellent和Dell EqualLogic存储阵列。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">戴尔服务器解决方案执行总监Brian Payne表示：“Emulex OneConnect 10GbE CNA是需要高密度性能的Dell PowerEdge客户的理想选择，能够为云、融合式基础架构和虚拟化部署提供良好支持。借助更高带宽、更低延时、存储卸载，以及高效简化数据中心内部和数据中心之间的VM迁移等优势，这一全新适配器产品系列能够全面满足我们客户的存储和网络需求。”</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">Emulex OneConnect万兆以太网CNA提供了一整套强大的特性和能力，包括：</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">通过Dell NPAR NIC分区技术优化带宽：Dell NPAR技术允许在每个网络适配器卡端口上创建多种PCI功能。作为一款CNA，Emulex OneConnect 万兆以太网CNA上的每个端口都可以配置成4个NIC功能，或者3个NIC功能和1个iSCSI（或FCoE）存储功能。NPAR是虚拟服务器环境的理想选择，因为它可以对带宽分配进行优化，从而支持I/O密集型应用、虚拟化服务器，以及服务器管理功能。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">系统管理简单灵活：Emulex OneConnect万兆以太网CNA兼容集成式Dell Remote Access Controller （iDRAC），iDRAC配备有Lifecycle Controller系统管理解决方案。iDRAC7采用了Lifecycle Controller技术，使管理员能够从任意地点对戴尔服务器进行部署、监控、管理、配置、更新、故障排查和修复，而无需使用代理。使用OneCommand Manager从单一控制台就可以控制、配置和管理适配器。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">高性能虚拟化：Emulex OneConnect万兆以太网CNA采用了高效、可扩展的硬件卸载技术，可以卸载虚拟网络开销。用于VMware VirtualWire连接时，与标准NIC相比可以把CPU利用率降低最高50%1，因此可以提高每台服务器支持的VM数。Emulex OneConnect万兆以太网CNA在处理小数据包时可提供4倍的网络性能2，轻松扩展事务密集型应用和集群应用。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">开放式支持软件定义网络：Emulex SURF开放式API提供了必要的工具来实施SDN技术，该技术可根据OpenStack和OpenFlow等下一代应用和新行业标准进行优化。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">快速、安全、可扩展的混合云连接：在支持新OVN标准，如Microsoft Hyper-V网络虚拟化所采用的Network Virtualization using Generic Routing Encapsulation (NVGRE)和VMware NSX中使用的Virtual Extensible LAN （VXLAN）时，与仅采用软件实现的解决方案相比，Emulex Virtual Network Exceleration（VNeX）卸载技术可以提供高出70%的性能1。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">通过FCoE和iSCSI硬件卸载提高存储连接灵活性：Emulex OneConnect万兆以太网CNA支持FCoE卸载，它使用了与Emulex LightPulse光纤通道HBA相同的Emulex驱动程序，而且经过了实践验证。Emulex OneConnect万兆以太网CNA还支持硬件iSCSI卸载，可以通过数据中心桥接（DCB）以太网fabric传输存储流量，其性能大大优于基于软件发起端和标准NIC的解决方案。</p>', '1', '', '', '1552822493', '', '', '25', '17', '');
INSERT INTO `cd_content` VALUES ('18', 'Emulex为惠普ProLiant交付16GFC夹层HBA', '9', '', '<p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">近日， Emulex宣布为惠普推出HP-LPe1605双通道第五代16Gb光纤通道（16GFC）夹层卡，它是面向HP BladeSystem C-Class Gen8平台的完美解决方案。这种卡基于8核心设计的Emulex LightPulse 第五代光纤通道（FC）HBA技术，能够提供最高的性能，安装管理更简单，具有无可匹敌的可扩展性和行业领先的虚拟化支持能力。HP-LPe1605拥有强大的管理工具，能够无缝集成到HP Storage Essentials （SE）、System Insight Manager（SIM）中，并且支持HP ProLiant Gen8 ProActive Insight Architecture，可以为多种多样的应用和环境提供最高性能。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">Emulex功能丰富的开发和管理工具可以简化HBA部署和设备管理帮助降低管理成本，保护IT投资。此外，在博科Gen 5 FC环境下Emulex ClearLinkTM 技术可以进行电缆和链路故障诊断。以下是这种新夹层卡适配器的主要特性和优势：</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"><strong>主要优势</strong></p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">• 完美适用于高带宽云部署和存储密集型应用，吞吐速度16GFC，延时更低，单端口每秒I/O操作数（IOPS）最高120万。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">• 在8GFC模式下，与其他8Gb FC适配器相比可以提供更高的IOPS和更低延时。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">• 卓越的质量和吞吐速度诊断功能，可以确保高可靠性和数据可用性。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">• 支持HP ProLiant Gen8 ProActive Insight架构。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">• 高效的安装流程和强大的互操作性，可以简化SAN软硬件的部署和升级。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"><strong>主要特性</strong></p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">• 单端口120万IOPS，比其他第五代HBA高20%。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">• 8核心ASIC能够支持最高的虚拟机（VM）密度，可以实现8192个并行登录/开放交换——最高可达其他适配器的4倍。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">• 支持1024个Message Signal Interrupts Extended（MSI-X），提高了主机利用率和应用性能。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">• 在配备了NPIV以及高达255个虚拟端口时，SAN可为虚拟服务器交付最佳性能。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">• 可以在SAN上的任意地点通过强大的管理应用高效集中管理Emulex HBA。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">• 最新支持基于预启动UEFI的配置，包括UEFI安全启动。</p>', '1', '', '', '1552822505', '', '', '21', '18', '');
INSERT INTO `cd_content` VALUES ('19', 'Emulex宣布最新产品将支持OCP开放计算项目', '9', '', '<p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">新工作负载，以及客户使用基础架构的方式已经再次发生变化，甚至网络流量围绕数据中心进行传输的基本框架也在改变，因此我们的架构解决方案也必须与时俱进，以便帮助最新的IBM System x3850/x3950 X6服务器、存储和网络中心组件全面实现，并平衡投资回报（ROI）。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">Emulex本季度的动作非同凡响，我们推出了自己的新OneConnect OCe14000系列万兆和四万兆以太网（10/40GbE）网络适配器和融合网络适配器（CNA），这些产品都基于新一代的Emulex Engine（XE）100系列I/O控制器。我们对行业最新的发展趋势和方向进行了深入研究，认识到了哪些趋势将在未来几年流行不衰，以及它们将对采用Emulex技术的IBM及我们的最终客户产生什么的影响。Emulex希望了解现实，而且有能力把握现实。Emulex的卓越产品可以改变客户设计解决方案的方式，更好地为企业负载、web级应用、虚拟环境和软件定义网络（SDN）部署提供支持。全新Emulex Virtual Fabric Adapter (VFA) 5 for IBM System x在三个维度上提高了性能：带宽更高、延时更低、每秒I/O操作数（IOPS）更快，从而能够解决流量和全球存储爆炸性增长的难题。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">2009年以来，Emulex VFA技术一直在帮助企业降低系统成本和复杂度，并提高性能。此次新推出的Emulex VFA5 for IBM System x继往开来，新增加了多项功能和特性，将再次彻底改变游戏，就像Emulex在2009年推出VFA技术时一样。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">Emulex VFA5 for IBM System x专门为满足企业、云服务提供商和电信公司的需求进行了优化，具有一系列强大的功能和特性，包括：</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">高性能虚拟化：Emulex VFA5采用了高效、可扩展的硬件卸载技术来卸载虚拟网络开销。在用于VMware VirtualWire连接时，与标准NIC相比它可提供高出50%的CPU利用率，因此可以增加每台服务器支持的虚拟机（VM）数量。此外，VFA 5在处理小数据包时可把网络性能真正提高4倍，从而满足扩展事务密集型应用和集群应用的性能需求。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">快速、安全、可扩展的混合云连接：与新Virtual Network Fabric标准的软件实现方式相比，Emulex Virtual Network Exceleration™（VNeX）卸载技术可提供高出70%的性能。新Virtual Network Fabric标准包括Microsoft Hyper-V网络虚拟化使用的NVGRE，以及VMware的NSX所使用的VXLAN （Virtual Extensible LAN，虚拟可扩展局域网)。它们都通过实现灵活的虚拟工作负载移动性来克服传统数据中心网络的限制，将虚拟机部署或网络重配时间从数天缩短到几分钟。与Microsoft Dynamic VMQ或VMware NetQueue一同使用时，Emulex VFA5可提供业内最先进的平台来提高虚拟机密度。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">使用先进的RoCE架构实现应用交付：Emulex VFA5基于低延时RoCE（RDMA over Converged Ethernet）架构，可帮助企业IT及云数据中心优化面向VDI、大数据、下一代NoSQL、内存数据库（in-memory databases）及传统企业IT工作负载的应用交付。这些新适配器将支持Windows Server SMB Direct和Linux NFS协议。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">块协议性能更高：相对于上一代VFA，Emulex VFA5把整体块协议每秒I/O操作数（IOPS）提高了50%，并继承了Emulex久经验证的企业级存储可靠性。</p>', '1', '', '', '1552822533', '', '', '32', '19', '');
INSERT INTO `cd_content` VALUES ('20', 'PMC亮相IDF 展示12G SAS分层存储方案', '9', '', '<span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">引领大数据连接、传送以及存储，提供创新半导体及软件解决方案的PMC公司出席在深圳举办的2014 IDF英特尔开发者论坛。此次，PMC将在 1层展示大厅的159 展台展出其带有maxCache Plus分层软件的Adaptec by PMC 8系列RAID卡</span><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">及6G SAS 配置的7系列RAID 以及HBA 卡。</span><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif; text-align: center;\"><img alt=\"PMC亮相IDF 展示12G SAS分层存储方案\" src=\"http://image20.it168.com/201404_800x800/1789/fbf6f4e720f56de4.jpg\" border=\"1\" style=\"padding: 0px; margin: 0px; border: 0px;\" /></p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif; text-align: center;\">maxCache Plus软件</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">数据中心需要高密度、灵活度以及优异的性能以满足现今大数据应用，而这些都是传统数据中心解决方案无法提供的。Adaptec by PMC将动态展示配备在8系列RAID卡上的maxCache Plus分层存储如何将性能提升50倍，这对于存储系统管理者满足当前需求至关重要。展示的形式将由两个对照组形成，让参观者对maxCache有更直观的认识。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">8系列中的maxCache Plus在管理与其连接的PCIe闪存卡、 SSD以及HDD的同时，可以实现对不同存储介质的分层，以确保关键应用和热数据优先使用高性能的存储介质，由此显著提升IOPS.它的加入也使得混合阵 列能够在RAID卡层完成分层，充分体现8系列的灵活性。演示中将会展示一款由12Gb/s SAS SSD和HDD组成的两层存储解决方案。其中性能分层是采用Seagate的1200 12Gb/s SAS SSD，而通过maxCache Plus驱动SSD加速第二层的Seagate 企业级Constellation.2 SATA HDD。Adaptec maxCache Plus的分层功能及冗余存储池均由Adaptec的maxView Storage Manager(存储管理员)进行管理。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">　　PMC也将推出出全新的12Gb/s 的4端口RAID卡(ASR-8405将于5月上市), 适用于较高密度、更快性能和更多配置的存储环境，可直接实现更快的数据交付和接入。富于创新的8系列卡能够帮助数据中心的架构师从存储资产中获得最高价值和性能表现。</p>', '1', '', '', '1552822562', '', '', '71', '20', '');
INSERT INTO `cd_content` VALUES ('21', 'LSI增强MgaRAID闪存卡更贴近超大规模环境', '9', '', '<p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">LSI已经将MegaRAID闪存卡的闪存容量从1.6TB翻了一番，并将端口数增加到16个，这样强大的小闪卡就可以用于在超大规模环境中加速服务器。</p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">连接PCIe的Nytro MegaRAID闪存卡将一个RAID控制器与板载闪存及缓存软件结合，提供对热数据的更快速访问。LSI表示，这个闪存卡用于“对磁盘数量和容量有较高要求的横向扩展服务器和存储环境”，提到了云和托管公司，以及类似的超大规模环境。</p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">MegaRAID 8140-8e8i PCIe闪存加速卡有4个闪存模块和16个SAS/SATA接口：这是目前可用端口数的4倍。该卡“将一个扩展器集成到架构中，提供横向扩展的服务器环境，最多可连接236个SAS和SATA设备，通过8个外置和内置的端口”。</p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">LSI表示，该卡的闪存能够以三种方式分区：</p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">- 针对数据量提供具有闪存速度的主存储</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">- 通过为定义容量运用RAID来提供数据保护</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">- 提供启动盘——LSI表示这样可以针对容量用途释放硬盘插槽</p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">高级缓存统计工具（Advanced Cache Statistics）提供了“诊断”功能，旨在确保产品环境实现理想的缓存优势，基于其应用和工作负载，并且提供了：</p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">- 从缓存完成的I/O数量</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">- 写入到缓存的热数据量</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">- 总I/O量，针对读取和写入，由主机发出</p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif; text-align: center;\"><a href=\"http://img.zdnet.com.cn/3/55/liXQNYaoC2ABo.jpg\" style=\"color: rgb(51, 51, 51); text-decoration-line: none;\"><img alt=\"LSI增强MgaRAID闪存卡更贴近超大规模环境\" src=\"http://img.zdnet.com.cn/3/55/liXQNYaoC2ABo.jpg\" border=\"0\" style=\"padding: 0px; margin: 0px; border: 0px;\" /></a></p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif; text-align: center;\"><span style=\"color: rgb(255, 0, 0);\">LSI Nytro MegaRAID 8140-8e8i闪存卡</span></p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">LSI表示，这项报告功能让用户可以在他们的设置环境中测量缓存的有效性。</p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">自从2012年月首次推出该产品，到目前为止LSI已经累计出货10万块Nytro PCIe闪存卡。LSI Nytro MegaRAID 8140-8e8i闪存卡应该是从2014年第二季度开始通过OEM和其他渠道合作伙伴生产出货。</p>', '1', '', '', '1552822591', '', '', '111', '21', '');
INSERT INTO `cd_content` VALUES ('22', '英特尔收购QLogic旗下InfiniBand业务', '9', '', '<p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">英特尔公司(INTC)周一宣布，已同意以1.25亿美元现金收购网络设备制造商QLogic Corp(QLGC)的InfiniBand部门，以加强其网络及高性能计算能力。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">英特尔表示，此项交易预计将于第一季度完成，并且表示，“此项收购旨在加强英特尔的网络资产组合，并提供可升级的高性能计算架构技术。”</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">该公司表示，预计大量InfiniBand员工将加入英特尔。</p><p style=\"padding: 5px 0px; margin: 0px; line-height: 24px; font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">QLogic表示，预计此项交易不会对其每股盈利产生影响。</p>', '1', '', '', '1552822615', '', '', '280', '22', '');
INSERT INTO `cd_content` VALUES ('23', '概论高清、网络化视频存储要求', '9', '', '<p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\">对于视频监控而言，图像清晰度无疑是最关键的特性。图像越清晰，细节越明显，观看 体验越好，智能等应用业务的准确度也越高。所以图像清晰度是视频监控永恒的追求。然而作为高清的视频，动辄几G到几十G的文件大小，这么大的视频文件，而 且有如潮水般的涌现，不仅对存储容量，对读写性能、可靠性等都提出了更高要求。因此，选择什么样的存储系统和方案，往往成为影响视频读写速度的关键。</span><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>高清、网络化视频存储要求</p><p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>1、在了解高清存储系统之前，必须知道什么是高清?</p><p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>在高清视频标准中，视频从最低标准到较高标准依次为720线非交错式，即720p逐行扫描;1080线交错式，即1080i隔行扫描;1080线非交错式，即1080p逐行扫描，屏幕纵横比为16:9，如果是视音频同步的HDTV，标准输出为杜比5.1声道数字格式。</p><p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>高 清视频有常见的三种分辨率，分别是：720P(1280×720P)逐行，美国的部分高清电视台主要采用这种格式;1080i(1920×1080i)隔 行;1080P(1920×1080P)逐行。网络视频高清资源以720P和1080i最为常见，其中作为视频监控系统的高清部分，已产品化的设备标准普 遍采用720P和1080P的拍摄标准。</p><p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>2、视频存储要求之大容量，即高清的文件到底有多大?</p><p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>高 清视频在经过不同的编码处理以后，依据码率不同，而有不同的要求。一般码率在6-20Mb之间，压缩效率、压缩方式不同，所获得的最终文件大小约 为：3-10GB/小时，因此便产生了对于存储大容量的要求。当然一般意义上的视频，压缩模式不同，占用的存储空间非常小，这里主要讨论一下高清视频的存 储容量。</p><p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>高 清视频的一种应用是提供这些高清网络视频资源下载的高清网站，规模比较小的站点片库中也会有成百上千部电影，这一类的网站在互联网上多如牛毛，而每个站点 存储系统的净容量要求至少在几十T，加上某些站点要建立多个文件映射和下载种子以提高综合流量，容量就不仅仅是几十个T了。</p><p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>另 一种应用是高清视频监控，虽然出于经济性考虑，此种应用中高清监控视频压缩率会比较高。目前720P高清视频摄像资料每小时视频录像可压缩到3GB左右容 量，但由于采集的是高清视频，而一般的监控系统摄像路数都是几百乃至上千路，所以这种应用将需要更多的存储设备和更大的存储容量。以此为例，按一个月保存 时间要求计算，可以得到这样一个数据：</p><p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>3GB/小时×24小时×30天×1路=2.16T</p><p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>每存储一路视频需要2T以上的净容量，那么计算一个拥有500路高清视频摄像，需要保存30天的监控系统所需的最少存储容量是1PB。</p><p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>3、视频存储要求之高性能</p><p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>众 所周知，除了BT分布式下载结构的网站，支持高清视频的效果大多是以服务端大数据流量为代价的。以每路数据流为20Mb的高清视频为例，在千兆单点服务模 式，最多可以容纳50路高清视频同时播放。当然这个是理论值，实际上还要考虑网络在处理数据撞包等任务时，消耗网络带宽资源之类的因素。因此，在高清视频 网站考虑服务时，首先要考虑向服务器提供高清视频数据的存储系统，扩大存储系统的带宽，速度才能得到有效的提升。在高清视频监控系统中，存储的传输速率要 求会随着监控系统的规模呈正比增长。</p><p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>4、视频存储要求之可靠性</p><p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>从 高清视频文件对用户的重要性来讲有几个不同的层次：一般安全性用户、中度安全性用户、重要视频数据用户。作为一般安全性用户，主要是指一些以分布式下载的 高清电影网站，他们对于高清数据安全性要求相对不高。偶尔存储系统离线，并不会对整个体系造成太大影响，但是对于数据的完整性要求比较高。</p><p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>中度安全性用户，如一些大型高清视频在线网站、交互网站等，要求高清视频数据具有实时性、交互性特点的，以及关键性的高清视频数据存储，都属于中度安全要求的用户。他们对于高清视频存储安全性的要求，是实时性和可恢复性。</p><p><span style=\"font-family: &quot;Microsoft Yahei&quot;, Arial, Helvetica, sans-serif;\"></span></p><p>重要视频数据用户，比如高清视频监控图像、媒体资讯制作内容等都属于重要视频数据类型，这类用户对高清视频存储安全的要求是实时性、冗余性和不间断性。</p>', '1', '', '', '1552822634', '', '', '641', '23', '');
INSERT INTO `cd_content` VALUES ('24', '人才招聘', '10', '', '内容建设中', '1', '', '', '1552823087', '', '', '', '24', '');
INSERT INTO `cd_content` VALUES ('25', '联系我们', '12', '', '<p><strong>深圳XXXX电子有限公司</strong></p><p>地址：深圳市福田区深南中路3037南光捷佳大厦605</p><p>联系人：XXXX</p><p>电话：0755-12345677</p><p>网址：<a href=\"http://www.xhadmin.cn\" target=\"_blank\">www.xhadmin.com</a></p>', '1', '', '', '1552823105', '', '', '', '25', '');
INSERT INTO `cd_content` VALUES ('27', 'banner2', '19', '/uploads/admin/1564382199734.jpg', '', '1', '', '', '1552827574', '', '', '', '27', '');
INSERT INTO `cd_content` VALUES ('28', 'banner3', '19', '/uploads/admin/15643821937328.jpg', '', '1', '', '', '1552827584', '', '', '', '28', '');
INSERT INTO `cd_content` VALUES ('29', 'LSI 9380-8i8e 阵列卡', '13', '/uploads/admin/1552831685383.jpg', '', '1', '1', '', '1552831666', '', '', '6', '29', '');
INSERT INTO `cd_content` VALUES ('30', 'LSI 9361-24i 阵列卡', '13', '/uploads/admin/15528317119183.jpg', '', '1', '', '', '1552831689', '', '', '5', '30', '');
INSERT INTO `cd_content` VALUES ('31', 'LSI 9361-16i 阵列卡', '13', '/uploads/admin/15528317381629.jpg', '', '1', '', '', '1552831720', '', '', '10', '31', '');
INSERT INTO `cd_content` VALUES ('33', 'LSI 9286-8e 外接阵列卡', '13', '/uploads/admin/15528317842911.jpg', '', '1', '', '', '1552831766', '', '', '5', '33', '');
INSERT INTO `cd_content` VALUES ('34', '3Ware 9750-8i 阵列卡', '13', '/uploads/admin/15528318038134.jpg', '', '1', '', '', '1552831792', '', '', '12', '34', '');
INSERT INTO `cd_content` VALUES ('35', 'LSI 9380-4i4e 内外接卡', '13', '/uploads/admin/15643823934765.jpg', '', '1', '', '', '1552831812', '', '', '3', '35', '');
INSERT INTO `cd_content` VALUES ('36', 'LSI 9271-8i 阵列卡', '13', '/uploads/admin/15643823678944.jpg', '', '1', '', '', '1552831842', '', '', '18', '36', '');
INSERT INTO `cd_content` VALUES ('57', '产品标题', '13', '/uploads/admin/15643823512188.jpg', '', '1', '', '', '1555993345', '', '', '9', '57', '');
INSERT INTO `cd_content` VALUES ('58', '产品标题', '13', '/uploads/admin/15643823198838.jpg', '', '1', '', '', '1555997683', '', '', '9', '58', '');
INSERT INTO `cd_content` VALUES ('60', '产品标题', '13', '/uploads/admin/15643822928663.jpg', '', '1', '', '', '1555997860', '', '', '5', '60', '');
INSERT INTO `cd_content` VALUES ('61', '产品标题', '13', '/uploads/admin/15643822466747.jpg', '', '1', '', '', '1555997903', '', '', '8', '61', '');
INSERT INTO `cd_content` VALUES ('62', '产品标题', '13', '/uploads/admin/15643822374494.jpg', '', '1', '', '', '1555997926', '', '', '22', '62', '');
INSERT INTO `cd_content` VALUES ('64', '产品标题', '13', '/uploads/admin/15643821175288.jpg', '', '1', '1', '', '1555997987', '', '', '36', '64', '');
INSERT INTO `cd_content` VALUES ('65', '产品标题', '13', '/uploads/admin/1564382105791.jpg', '', '1', '1', '', '1555998191', '', '', '33', '65', '');
INSERT INTO `cd_content` VALUES ('66', '产品标题', '13', '/uploads/admin/15643820923760.jpg', '', '1', '1,2', '', '1555998214', '', '', '121', '66', '');
INSERT INTO `cd_content` VALUES ('68', '测试信息1', '13', '/uploads/admin/15643820838507.jpg', '<p>当时都是多所</p>', '1', '2', '', '1514766819', '', '', '126', '68', '');
INSERT INTO `cd_content` VALUES ('72', '测试标题', '13', '/uploads/admin/15643798913075.jpg', '<p>测试内容</p>', '1', '1', '', '1556683144', '', '', '206', '72', 'admin');
INSERT INTO `cd_content` VALUES ('73', '新闻资讯缓存测试', '9', '', '<p>单身的等等丰富的</p>', '1', '', '', '1572327950', '', '', '9', '73', '');
INSERT INTO `cd_content` VALUES ('74', '公司简介（自定义的内容模型）', '7', '/uploads/admin/201910/5db7db38b570d.jpg', '<p><span style=\"font-size: 24px;\">本系统所有的数据管理模块 以及前端接口 均由xhadmin生成 xhamdin&nbsp;</span></p><p><span style=\"font-size: 24px;\"><br/></span></p><p><span style=\"font-size: 24px;\">体验地址 </span><a href=\"http://test2.xhadmin.com\" _src=\"http://test2.xhadmin.com\" style=\"font-size: 24px; text-decoration: underline;\"><span style=\"font-size: 24px;\">http://test2.xhadmin.com</span></a><span style=\"font-size: 24px;\">&nbsp;</span></p><p><span style=\"font-size: 24px;\"><br/></span></p><p><span style=\"font-size: 24px;\">视频教程：</span><a href=\"http://www.xhadmin.com/html/shipinjiaocheng/index.html\" style=\"font-size: 24px; text-decoration: underline;\"><span style=\"font-size: 24px;\">http://www.xhadmin.com/html/shipinjiaocheng/index.html</span></a></p>', '1', '', '', '1572330298', '', '', '', '74', '');

-- ----------------------------
-- Table structure for `cd_ext_baoming`
-- ----------------------------
DROP TABLE IF EXISTS `cd_ext_baoming`;
CREATE TABLE `cd_ext_baoming` (
  `data_id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) DEFAULT NULL,
  `mobile` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `province` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `district` varchar(250) DEFAULT NULL,
  `sex` tinyint(4) DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `img` varchar(250) DEFAULT NULL,
  `xheditor` text,
  `ueditor` text,
  `images` text,
  PRIMARY KEY (`data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_ext_baoming
-- ----------------------------
INSERT INTO `cd_ext_baoming` VALUES ('6', 'heyingmin', '13545028417', '2143574@qq.com', '湖北省', '鄂州市', '鄂城区', '1', '1572274912', '/uploads/admin/201910/5db704f259231.jpg', '测试内容', '<p>测试内容<br/></p>', '/uploads/admin/201910/5db7ae36bfb82.jpg|/uploads/admin/201910/5db7ae36b17de.jpg|/uploads/admin/201910/5db7ae35c701f.jpg|/uploads/admin/201910/5db7ae35b0c35.jpg|');
INSERT INTO `cd_ext_baoming` VALUES ('7', 'heyingmin', '13545028477', '214363574@qq.com', '湖北省', '鄂州市', '鄂城区', '1', '1572314961', '/uploads/admin/201910/5db704f259231.jpg', null, null, null);
INSERT INTO `cd_ext_baoming` VALUES ('8', 'heyingmin', '13545028472', '274363574@qq.com', '湖北省', '鄂州市', '华容区', '1', '1572314961', '/uploads/admin/201910/5db7066625ee3.jpg', null, null, null);

-- ----------------------------
-- Table structure for `cd_ext_book`
-- ----------------------------
DROP TABLE IF EXISTS `cd_ext_book`;
CREATE TABLE `cd_ext_book` (
  `data_id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) DEFAULT NULL,
  `mobile` varchar(250) DEFAULT NULL,
  `qq` varchar(250) DEFAULT NULL,
  `content` text,
  `create_time` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_ext_book
-- ----------------------------
INSERT INTO `cd_ext_book` VALUES ('7', '寒塘冷月', '13515028470', '274363574', '留言测试内容', '1572329817', '274363574@qq.com', '0');
INSERT INTO `cd_ext_book` VALUES ('8', '梦笑曾经', '13541028470', '67766767111', '留言测试内容', '1572329844', '274313574@qq.com', '1');
INSERT INTO `cd_ext_book` VALUES ('9', '一方', '13525028470', '67766767', '留言测试', '1572329942', '274363574@qq.com', '0');

-- ----------------------------
-- Table structure for `cd_ext_case`
-- ----------------------------
DROP TABLE IF EXISTS `cd_ext_case`;
CREATE TABLE `cd_ext_case` (
  `data_id` int(10) NOT NULL AUTO_INCREMENT,
  `content_id` int(100) DEFAULT NULL,
  `des` varchar(250) DEFAULT NULL,
  `file` text,
  `images` text,
  PRIMARY KEY (`data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_ext_case
-- ----------------------------
INSERT INTO `cd_ext_case` VALUES ('3', '11', '描述信息测试1111', '/uploads/admin/201910/5db6ff924eae9.jpg', '/upload/15487707418794.jpg|/upload/15487707412979.jpg|/upload/15487707419454.jpg|/upload/15487707416558.jpg|');
INSERT INTO `cd_ext_case` VALUES ('4', '74', '描述信息测试1111', '/uploads/admin/201910/5db7db270dce8.csv', '/uploads/admin/201910/5db7db33d9fb9.jpg|/uploads/admin/201910/5db7db33cbe27.jpg|/uploads/admin/201910/5db7db32dd6b7.jpg|');

-- ----------------------------
-- Table structure for `cd_extend`
-- ----------------------------
DROP TABLE IF EXISTS `cd_extend`;
CREATE TABLE `cd_extend` (
  `extend_id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `table_name` varchar(250) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `sortid` tinyint(4) DEFAULT NULL COMMENT '排序',
  `action` varchar(50) DEFAULT NULL COMMENT '操作方法',
  `orderby` varchar(50) DEFAULT NULL COMMENT '默认排序',
  `is_post` tinyint(4) DEFAULT NULL COMMENT '是否允许投稿',
  PRIMARY KEY (`extend_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_extend
-- ----------------------------
INSERT INTO `cd_extend` VALUES ('38', '在线留言', 'book', '1', '2', '100', 'add,update,delete,view', '', '1');
INSERT INTO `cd_extend` VALUES ('39', '案例', 'case', '1', '1', '100', '', null, '0');
INSERT INTO `cd_extend` VALUES ('40', '在线报名', 'baoming', '1', '2', '100', 'add,update,delete,view,dumpData,importData', 'data_id asc', '1');

-- ----------------------------
-- Table structure for `cd_field`
-- ----------------------------
DROP TABLE IF EXISTS `cd_field`;
CREATE TABLE `cd_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extend_id` int(9) NOT NULL COMMENT '模块ID',
  `name` varchar(64) NOT NULL COMMENT '字段名称',
  `field` varchar(32) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '表单类型1输入框 2下拉框 3单选框 4多选框 5上传图片 6编辑器 7时间',
  `list_show` tinyint(4) NOT NULL COMMENT '列表显示',
  `align` varchar(12) DEFAULT NULL,
  `is_search` tinyint(4) DEFAULT NULL COMMENT '是否搜索',
  `config` varchar(255) DEFAULT NULL COMMENT '下拉框或者单选框默认值',
  `note` varchar(255) DEFAULT NULL COMMENT '提示信息',
  `message` varchar(255) DEFAULT NULL COMMENT '错误提示',
  `validate` varchar(32) DEFAULT NULL COMMENT '验证方式',
  `rule` mediumtext COMMENT '验证规则',
  `sortid` mediumint(9) DEFAULT '0' COMMENT '排序号',
  `default_value` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `extend_id` (`extend_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=698 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_field
-- ----------------------------
INSERT INTO `cd_field` VALUES ('677', '38', '姓名', 'username', '1', '1', '', '1', '', '', '', 'notEmpty', '', '677', '', '1');
INSERT INTO `cd_field` VALUES ('697', '38', '状态', 'status', '3', '1', '', '1', '通过|1|success,禁用|0|danger', '', '', '', '', '697', '0', '1');
INSERT INTO `cd_field` VALUES ('679', '39', '说明', 'des', '1', '1', '', '1', '', '', '', '', '', '679', '', '1');
INSERT INTO `cd_field` VALUES ('680', '39', '上传', 'file', '10', '1', '', '1', '', '', '', '', '', '680', '', '1');
INSERT INTO `cd_field` VALUES ('681', '39', '图集', 'images', '9', '1', '', '1', '', '', '', '', '', '681', '', '1');
INSERT INTO `cd_field` VALUES ('682', '40', '姓名', 'username', '1', '1', '', '1', '', '', '', '', '', '682', '', '1');
INSERT INTO `cd_field` VALUES ('683', '40', '电话', 'mobile', '1', '1', '', '1', '', '', '', 'notEmpty,unique', '/^1[34578]\\d{9}$/', '683', '', '1');
INSERT INTO `cd_field` VALUES ('684', '40', '邮箱', 'email', '1', '1', '', '1', '', '', '', 'notEmpty,unique', '/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/', '684', '', '1');
INSERT INTO `cd_field` VALUES ('685', '40', '省市区', 'province|city|district', '17', '1', '', '1', '', '', '', '', '', '685', '', '1');
INSERT INTO `cd_field` VALUES ('686', '40', '性别', 'sex', '3', '1', '', '1', '男|1|primary,女|2|warning', '', '', '', '', '686', '', '1');
INSERT INTO `cd_field` VALUES ('687', '40', '创建时间', 'create_time', '12', '1', '', '1', '', '', '', '', '', '687', '', '1');
INSERT INTO `cd_field` VALUES ('688', '40', '头像', 'img', '8', '1', '', '1', '', '', '', '', '', '688', '', '1');
INSERT INTO `cd_field` VALUES ('689', '40', '编辑器', 'xheditor', '11', '0', '', '0', '', '', '', '', '', '689', '', '1');
INSERT INTO `cd_field` VALUES ('690', '40', 'ueditor', 'ueditor', '16', '0', '', '0', '', '', '', '', '', '690', '', '1');
INSERT INTO `cd_field` VALUES ('691', '40', '图集', 'images', '9', '0', '', '0', '', '', '', '', '', '691', '', '1');
INSERT INTO `cd_field` VALUES ('696', '38', '电子邮箱', 'email', '1', '1', '', '1', '', '', '', '', '/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/', '693', '', '1');
INSERT INTO `cd_field` VALUES ('692', '38', '电话', 'mobile', '1', '1', '', '1', '', '', '', 'notEmpty', '/^1[34578]\\d{9}$/', '692', '', '1');
INSERT INTO `cd_field` VALUES ('693', '38', 'qq', 'qq', '1', '1', '', '1', '', '', '', '', '/^[0-9]*$/', '694', '', '1');
INSERT INTO `cd_field` VALUES ('694', '38', '留言内容', 'content', '6', '0', '', '0', '', '', '', '', '', '695', '', '1');
INSERT INTO `cd_field` VALUES ('695', '38', '留言时间', 'create_time', '12', '1', '', '1', '', '', '', '', '', '696', '', '1');

-- ----------------------------
-- Table structure for `cd_frament`
-- ----------------------------
DROP TABLE IF EXISTS `cd_frament`;
CREATE TABLE `cd_frament` (
  `frament_id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `pic` varchar(250) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`frament_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_frament
-- ----------------------------
INSERT INTO `cd_frament` VALUES ('1', '首页简介', null, '<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;深圳市XXXX电子有限公司办公室地址位于中国个经济特区，鹏城深圳，深圳深圳市福田区福虹路华强花园11栋602，于2014年03月03日在深圳市市场监督管理局注册...<a href=\"/html/gongsijianjie\">详细&gt;&gt;</a></p>');
INSERT INTO `cd_frament` VALUES ('2', '底部版权', null, '<span style=\"color: rgb(25, 25, 25); font-family: \" microsoft=\"\" yahei=\"\" arial=\"\" helvetica=\"\" sans-serif=\"\" text-align:=\"\" center=\"\">Copyright 2005-2019 武汉XXXXX电子有限公司 技术支持:寒塘冷月 qq:274363574 All rights reserved&nbsp; <a href=\"/admin\" target=\"_blank\">后台管理</a></span><br />');
INSERT INTO `cd_frament` VALUES ('3', '首页联系我们', null, '<p><strong>XX有好数电子商务有限公司</strong></p><p>地址：江苏南京市玄武区东大科技园1号楼</p><p>邮编：210018</p><p>电话：025 8472087119</p><p>网址：<a href=\"http://www.xhadmin.com\" target=\"_blank\">www.xhadmin.com</a></p>');

-- ----------------------------
-- Table structure for `cd_group`
-- ----------------------------
DROP TABLE IF EXISTS `cd_group`;
CREATE TABLE `cd_group` (
  `group_id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(36) DEFAULT NULL COMMENT '分组名称',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态 10正常 0禁用',
  `role` tinyint(4) DEFAULT NULL COMMENT '角色类别 1超级管理员 2普通管理员',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_group
-- ----------------------------
INSERT INTO `cd_group` VALUES ('1', '超级管理员', '10', '1');
INSERT INTO `cd_group` VALUES ('3', '客服人员', '10', '2');

-- ----------------------------
-- Table structure for `cd_link`
-- ----------------------------
DROP TABLE IF EXISTS `cd_link`;
CREATE TABLE `cd_link` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL COMMENT '连接标题',
  `url` varchar(250) DEFAULT NULL COMMENT '链接地址',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态',
  `sortid` int(10) DEFAULT NULL COMMENT '排序',
  `logo` varchar(250) DEFAULT NULL COMMENT 'logo',
  `cata_id` tinyint(4) DEFAULT NULL COMMENT '所属分类',
  `create_time` int(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_link
-- ----------------------------
INSERT INTO `cd_link` VALUES ('1', '百度', 'http://www.baidu.com', '1', '100', '/uploads/admin/201910/5db68b5d99901.jpg', '1', '1571760000');
INSERT INTO `cd_link` VALUES ('2', '新浪', 'http://www.baidu.com', '1', '100', '/uploads/admin/201910/5db68b585f700.jpg', '1', '1572192000');

-- ----------------------------
-- Table structure for `cd_link_catagory`
-- ----------------------------
DROP TABLE IF EXISTS `cd_link_catagory`;
CREATE TABLE `cd_link_catagory` (
  `cata_id` int(10) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(250) DEFAULT NULL COMMENT '分类名称',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`cata_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_link_catagory
-- ----------------------------
INSERT INTO `cd_link_catagory` VALUES ('1', '默认分类', '1');
INSERT INTO `cd_link_catagory` VALUES ('2', '底部链接', '1');

-- ----------------------------
-- Table structure for `cd_log`
-- ----------------------------
DROP TABLE IF EXISTS `cd_log`;
CREATE TABLE `cd_log` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(250) DEFAULT NULL,
  `event` varchar(250) DEFAULT NULL,
  `ip` varchar(250) DEFAULT NULL,
  `time` int(10) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=583 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_log
-- ----------------------------
INSERT INTO `cd_log` VALUES ('60', '10', 'test01', '用户登录', '127.0.0.1', '1567479480');
INSERT INTO `cd_log` VALUES ('62', '10', 'test01', '用户登录', '127.0.0.1', '1568010262');
INSERT INTO `cd_log` VALUES ('63', '10', 'test01', '用户登录', '127.0.0.1', '1568010297');
INSERT INTO `cd_log` VALUES ('65', '10', 'test01', '用户登录', '127.0.0.1', '1568010611');
INSERT INTO `cd_log` VALUES ('67', '10', 'test01', '用户登录', '127.0.0.1', '1568010726');
INSERT INTO `cd_log` VALUES ('71', '10', 'test01', '用户登录', '127.0.0.1', '1568014515');
INSERT INTO `cd_log` VALUES ('114', '1', 'admin', '用户登录', '127.0.0.1', '1571812547');
INSERT INTO `cd_log` VALUES ('115', '1', 'admin', '用户登录', '221.232.62.73', '1571817006');
INSERT INTO `cd_log` VALUES ('116', '1', 'admin', '用户登录', '27.18.38.166', '1571830046');
INSERT INTO `cd_log` VALUES ('117', '1', 'admin', '用户登录', '27.18.38.166', '1571842277');
INSERT INTO `cd_log` VALUES ('118', '1', 'admin', '用户登录', '27.18.38.166', '1571842363');
INSERT INTO `cd_log` VALUES ('119', '11', 'test01', '用户登录', '27.18.38.166', '1571845367');
INSERT INTO `cd_log` VALUES ('120', '1', 'admin', '用户登录', '27.18.38.166', '1571845413');
INSERT INTO `cd_log` VALUES ('121', '11', 'test01', '用户登录', '27.18.38.166', '1571846275');
INSERT INTO `cd_log` VALUES ('122', '1', 'admin', '用户登录', '27.18.38.166', '1571847218');
INSERT INTO `cd_log` VALUES ('123', '1', 'admin', '用户登录', '27.18.38.166', '1571847527');
INSERT INTO `cd_log` VALUES ('124', '1', 'admin', '用户登录', '27.18.38.166', '1571847925');
INSERT INTO `cd_log` VALUES ('125', '1', 'admin', '用户登录', '27.18.38.166', '1571848322');
INSERT INTO `cd_log` VALUES ('126', '1', 'admin', '用户登录', '120.82.231.197', '1571848940');
INSERT INTO `cd_log` VALUES ('127', '1', 'admin', '用户登录', '113.102.236.194', '1571850601');
INSERT INTO `cd_log` VALUES ('128', '1', 'admin', '用户登录', '106.19.220.78', '1571852919');
INSERT INTO `cd_log` VALUES ('129', '1', 'admin', '用户登录', '106.19.220.78', '1571853021');
INSERT INTO `cd_log` VALUES ('130', '1', 'admin', '用户登录', '42.236.200.129', '1571853679');
INSERT INTO `cd_log` VALUES ('131', '1', 'admin', '用户登录', '120.239.40.174', '1571858942');
INSERT INTO `cd_log` VALUES ('132', '1', 'admin', '用户登录', '115.202.229.100', '1571859113');
INSERT INTO `cd_log` VALUES ('133', '1', 'admin', '用户登录', '115.210.67.198', '1571860974');
INSERT INTO `cd_log` VALUES ('134', '1', 'admin', '用户登录', '115.210.67.198', '1571862033');
INSERT INTO `cd_log` VALUES ('135', '1', 'admin', '用户登录', '220.200.27.106', '1571863098');
INSERT INTO `cd_log` VALUES ('136', '1', 'admin', '用户登录', '183.192.0.30', '1571865680');
INSERT INTO `cd_log` VALUES ('137', '1', 'admin', '用户登录', '211.148.195.211', '1571872969');
INSERT INTO `cd_log` VALUES ('138', '1', 'admin', '用户登录', '112.20.83.115', '1571873565');
INSERT INTO `cd_log` VALUES ('139', '1', 'admin', '用户登录', '112.20.83.115', '1571873632');
INSERT INTO `cd_log` VALUES ('140', '1', 'admin', '用户登录', '163.125.179.191', '1571873959');
INSERT INTO `cd_log` VALUES ('141', '1', 'admin', '用户登录', '183.47.12.226', '1571876061');
INSERT INTO `cd_log` VALUES ('142', '1', 'admin', '用户登录', '182.127.60.26', '1571876756');
INSERT INTO `cd_log` VALUES ('143', '1', 'admin', '用户登录', '218.16.97.82', '1571877161');
INSERT INTO `cd_log` VALUES ('144', '1', 'admin', '用户登录', '36.22.216.185', '1571877373');
INSERT INTO `cd_log` VALUES ('145', '1', 'admin', '用户登录', '124.164.75.207', '1571877432');
INSERT INTO `cd_log` VALUES ('146', '1', 'admin', '用户登录', '182.240.233.0', '1571877699');
INSERT INTO `cd_log` VALUES ('147', '1', 'admin', '用户登录', '113.104.238.195', '1571877770');
INSERT INTO `cd_log` VALUES ('148', '1', 'admin', '用户登录', '1.62.118.254', '1571877917');
INSERT INTO `cd_log` VALUES ('149', '1', 'admin', '用户登录', '171.44.136.205', '1571877945');
INSERT INTO `cd_log` VALUES ('150', '1', 'admin', '用户登录', '49.70.173.207', '1571878059');
INSERT INTO `cd_log` VALUES ('151', '1', 'admin', '用户登录', '125.66.248.42', '1571878306');
INSERT INTO `cd_log` VALUES ('152', '1', 'admin', '用户登录', '182.46.204.129', '1571878390');
INSERT INTO `cd_log` VALUES ('153', '1', 'admin', '用户登录', '117.165.42.66', '1571879293');
INSERT INTO `cd_log` VALUES ('154', '1', 'admin', '用户登录', '117.22.22.81', '1571879530');
INSERT INTO `cd_log` VALUES ('155', '1', 'admin', '用户登录', '61.140.247.123', '1571879778');
INSERT INTO `cd_log` VALUES ('156', '1', 'admin', '用户登录', '180.112.142.181', '1571879892');
INSERT INTO `cd_log` VALUES ('157', '1', 'admin', '用户登录', '113.245.107.199', '1571879913');
INSERT INTO `cd_log` VALUES ('158', '1', 'admin', '用户登录', '1.80.233.65', '1571879969');
INSERT INTO `cd_log` VALUES ('159', '1', 'admin', '用户登录', '117.136.0.231', '1571880137');
INSERT INTO `cd_log` VALUES ('160', '1', 'admin', '用户登录', '113.65.160.152', '1571880352');
INSERT INTO `cd_log` VALUES ('161', '1', 'admin', '用户登录', '182.139.95.199', '1571880490');
INSERT INTO `cd_log` VALUES ('162', '1', 'admin', '用户登录', '113.116.118.134', '1571880494');
INSERT INTO `cd_log` VALUES ('163', '1', 'admin', '用户登录', '115.197.175.165', '1571880849');
INSERT INTO `cd_log` VALUES ('164', '1', 'admin', '用户登录', '219.157.201.150', '1571881129');
INSERT INTO `cd_log` VALUES ('165', '1', 'admin', '用户登录', '120.239.74.19', '1571881298');
INSERT INTO `cd_log` VALUES ('166', '1', 'admin', '用户登录', '171.8.196.78', '1571881468');
INSERT INTO `cd_log` VALUES ('167', '1', 'admin', '用户登录', '27.18.39.231', '1571881763');
INSERT INTO `cd_log` VALUES ('168', '1', 'admin', '用户登录', '223.88.151.39', '1571881961');
INSERT INTO `cd_log` VALUES ('169', '1', 'admin', '用户登录', '221.204.81.0', '1571882036');
INSERT INTO `cd_log` VALUES ('170', '1', 'admin', '用户登录', '221.204.81.0', '1571882100');
INSERT INTO `cd_log` VALUES ('171', '1', 'admin', '用户登录', '183.156.67.166', '1571882125');
INSERT INTO `cd_log` VALUES ('172', '1', 'admin', '用户登录', '220.176.82.2', '1571882228');
INSERT INTO `cd_log` VALUES ('173', '1', 'admin', '用户登录', '183.156.123.51', '1571882249');
INSERT INTO `cd_log` VALUES ('174', '1', 'admin', '用户登录', '182.32.182.221', '1571882443');
INSERT INTO `cd_log` VALUES ('175', '1', 'admin', '用户登录', '222.244.213.244', '1571882614');
INSERT INTO `cd_log` VALUES ('176', '1', 'admin', '用户登录', '118.180.214.197', '1571883181');
INSERT INTO `cd_log` VALUES ('177', '1', 'admin', '用户登录', '218.17.157.227', '1571883828');
INSERT INTO `cd_log` VALUES ('178', '1', 'admin', '用户登录', '14.154.31.51', '1571883851');
INSERT INTO `cd_log` VALUES ('179', '1', 'admin', '用户登录', '120.230.0.157', '1571884364');
INSERT INTO `cd_log` VALUES ('180', '1', 'admin', '用户登录', '113.109.161.255', '1571884393');
INSERT INTO `cd_log` VALUES ('181', '1', 'admin', '用户登录', '218.69.133.153', '1571884519');
INSERT INTO `cd_log` VALUES ('182', '1', 'admin', '用户登录', '114.246.98.207', '1571884650');
INSERT INTO `cd_log` VALUES ('183', '1', 'admin', '用户登录', '125.44.232.124', '1571885132');
INSERT INTO `cd_log` VALUES ('184', '1', 'admin', '用户登录', '43.228.180.84', '1571885573');
INSERT INTO `cd_log` VALUES ('185', '1', 'admin', '用户登录', '221.232.62.73', '1571885710');
INSERT INTO `cd_log` VALUES ('186', '1', 'admin', '用户登录', '221.222.125.161', '1571886457');
INSERT INTO `cd_log` VALUES ('187', '1', 'admin', '用户登录', '222.188.189.17', '1571886934');
INSERT INTO `cd_log` VALUES ('188', '1', 'admin', '用户登录', '113.81.224.204', '1571886946');
INSERT INTO `cd_log` VALUES ('189', '1', 'admin', '用户登录', '183.253.248.76', '1571887064');
INSERT INTO `cd_log` VALUES ('190', '1', 'admin', '用户登录', '221.232.62.73', '1571887140');
INSERT INTO `cd_log` VALUES ('191', '1', 'admin', '用户登录', '223.88.151.39', '1571887706');
INSERT INTO `cd_log` VALUES ('192', '1', 'admin', '用户登录', '221.204.81.0', '1571888606');
INSERT INTO `cd_log` VALUES ('193', '1', 'admin', '用户登录', '113.140.16.75', '1571889033');
INSERT INTO `cd_log` VALUES ('194', '1', 'admin', '用户登录', '218.205.57.19', '1571889347');
INSERT INTO `cd_log` VALUES ('195', '1', 'admin', '用户登录', '113.109.161.255', '1571889586');
INSERT INTO `cd_log` VALUES ('196', '1', 'admin', '用户登录', '221.232.62.73', '1571890660');
INSERT INTO `cd_log` VALUES ('197', '1', 'admin', '用户登录', '223.90.33.186', '1571890770');
INSERT INTO `cd_log` VALUES ('198', '1', 'admin', '用户登录', '183.198.16.98', '1571890971');
INSERT INTO `cd_log` VALUES ('199', '1', 'admin', '用户登录', '221.232.62.73', '1571893280');
INSERT INTO `cd_log` VALUES ('200', '1', 'admin', '用户登录', '211.196.191.86', '1571893463');
INSERT INTO `cd_log` VALUES ('201', '1', 'admin', '用户登录', '113.87.96.186', '1571895327');
INSERT INTO `cd_log` VALUES ('202', '1', 'admin', '用户登录', '221.204.81.0', '1571895467');
INSERT INTO `cd_log` VALUES ('203', '1', 'admin', '用户登录', '222.216.130.209', '1571895927');
INSERT INTO `cd_log` VALUES ('204', '1', 'admin', '用户登录', '221.232.62.73', '1571896185');
INSERT INTO `cd_log` VALUES ('205', '1', 'admin', '用户登录', '113.233.35.228', '1571897056');
INSERT INTO `cd_log` VALUES ('206', '1', 'admin', '用户登录', '113.233.35.228', '1571897101');
INSERT INTO `cd_log` VALUES ('207', '1', 'admin', '用户登录', '221.232.62.73', '1571899269');
INSERT INTO `cd_log` VALUES ('208', '1', 'admin', '用户登录', '58.215.200.58', '1571900392');
INSERT INTO `cd_log` VALUES ('209', '1', 'admin', '用户登录', '123.101.183.215', '1571900476');
INSERT INTO `cd_log` VALUES ('210', '1', 'admin', '用户登录', '113.140.16.75', '1571901664');
INSERT INTO `cd_log` VALUES ('211', '1', 'admin', '用户登录', '113.116.31.158', '1571902074');
INSERT INTO `cd_log` VALUES ('212', '1', 'admin', '用户登录', '120.41.171.136', '1571903598');
INSERT INTO `cd_log` VALUES ('213', '1', 'admin', '用户登录', '101.227.12.253', '1571903621');
INSERT INTO `cd_log` VALUES ('214', '1', 'admin', '用户登录', '183.130.14.54', '1571903706');
INSERT INTO `cd_log` VALUES ('215', '1', 'admin', '用户登录', '180.172.123.24', '1571904677');
INSERT INTO `cd_log` VALUES ('216', '1', 'admin', '用户登录', '220.172.54.124', '1571905007');
INSERT INTO `cd_log` VALUES ('217', '1', 'admin', '用户登录', '112.28.208.139', '1571905473');
INSERT INTO `cd_log` VALUES ('218', '1', 'admin', '用户登录', '112.198.9.10', '1571905637');
INSERT INTO `cd_log` VALUES ('219', '1', 'admin', '用户登录', '110.184.20.163', '1571905664');
INSERT INTO `cd_log` VALUES ('220', '1', 'admin', '用户登录', '113.140.16.75', '1571905983');
INSERT INTO `cd_log` VALUES ('221', '1', 'admin', '用户登录', '103.50.17.130', '1571906987');
INSERT INTO `cd_log` VALUES ('222', '1', 'admin', '用户登录', '113.66.5.253', '1571907229');
INSERT INTO `cd_log` VALUES ('223', '1', 'admin', '用户登录', '106.120.99.10', '1571907580');
INSERT INTO `cd_log` VALUES ('224', '1', 'admin', '用户登录', '120.41.123.163', '1571907787');
INSERT INTO `cd_log` VALUES ('225', '1', 'admin', '用户登录', '116.21.158.239', '1571908789');
INSERT INTO `cd_log` VALUES ('226', '1', 'admin', '用户登录', '114.222.186.56', '1571908856');
INSERT INTO `cd_log` VALUES ('227', '1', 'admin', '用户登录', '118.144.20.166', '1571909386');
INSERT INTO `cd_log` VALUES ('228', '1', 'admin', '用户登录', '119.123.66.119', '1571909654');
INSERT INTO `cd_log` VALUES ('229', '1', 'admin', '用户登录', '36.5.118.221', '1571910196');
INSERT INTO `cd_log` VALUES ('230', '1', 'admin', '用户登录', '183.15.176.40', '1571910634');
INSERT INTO `cd_log` VALUES ('231', '1', 'admin', '用户登录', '139.212.194.198', '1571910808');
INSERT INTO `cd_log` VALUES ('232', '1', 'admin', '用户登录', '115.60.81.129', '1571910879');
INSERT INTO `cd_log` VALUES ('233', '1', 'admin', '用户登录', '36.98.140.168', '1571911030');
INSERT INTO `cd_log` VALUES ('234', '1', 'admin', '用户登录', '171.11.23.245', '1571911697');
INSERT INTO `cd_log` VALUES ('235', '1', 'admin', '用户登录', '113.109.232.252', '1571912749');
INSERT INTO `cd_log` VALUES ('236', '1', 'admin', '用户登录', '27.18.39.231', '1571915348');
INSERT INTO `cd_log` VALUES ('237', '1', 'admin', '用户登录', '111.36.164.162', '1571916489');
INSERT INTO `cd_log` VALUES ('238', '1', 'admin', '用户登录', '112.251.192.45', '1571918520');
INSERT INTO `cd_log` VALUES ('239', '1', 'admin', '用户登录', '223.74.34.113', '1571921916');
INSERT INTO `cd_log` VALUES ('240', '1', 'admin', '用户登录', '42.234.16.24', '1571925133');
INSERT INTO `cd_log` VALUES ('241', '1', 'admin', '用户登录', '183.228.43.182', '1571925467');
INSERT INTO `cd_log` VALUES ('242', '1', 'admin', '用户登录', '113.105.12.154', '1571925970');
INSERT INTO `cd_log` VALUES ('243', '1', 'admin', '用户登录', '39.66.221.164', '1571926017');
INSERT INTO `cd_log` VALUES ('244', '1', 'admin', '用户登录', '110.246.61.239', '1571926789');
INSERT INTO `cd_log` VALUES ('245', '1', 'admin', '用户登录', '183.17.49.117', '1571926918');
INSERT INTO `cd_log` VALUES ('246', '1', 'admin', '用户登录', '59.63.204.192', '1571927780');
INSERT INTO `cd_log` VALUES ('247', '1', 'admin', '用户登录', '223.88.89.236', '1571928823');
INSERT INTO `cd_log` VALUES ('248', '1', 'admin', '用户登录', '110.229.26.106', '1571928893');
INSERT INTO `cd_log` VALUES ('249', '1', 'admin', '用户登录', '27.18.39.231', '1571931234');
INSERT INTO `cd_log` VALUES ('250', '1', 'admin', '用户登录', '110.246.61.239', '1571931719');
INSERT INTO `cd_log` VALUES ('251', '1', 'admin', '用户登录', '121.35.0.138', '1571933425');
INSERT INTO `cd_log` VALUES ('252', '1', 'admin', '用户登录', '27.215.92.157', '1571934249');
INSERT INTO `cd_log` VALUES ('253', '1', 'admin', '用户登录', '27.215.92.157', '1571934295');
INSERT INTO `cd_log` VALUES ('254', '1', 'admin', '用户登录', '120.230.77.170', '1571935354');
INSERT INTO `cd_log` VALUES ('255', '1', 'admin', '用户登录', '120.9.197.206', '1571938424');
INSERT INTO `cd_log` VALUES ('256', '1', 'admin', '用户登录', '113.88.159.221', '1571940258');
INSERT INTO `cd_log` VALUES ('257', '1', 'admin', '用户登录', '42.226.119.9', '1571964250');
INSERT INTO `cd_log` VALUES ('258', '1', 'admin', '用户登录', '124.237.89.254', '1571964826');
INSERT INTO `cd_log` VALUES ('259', '1', 'admin', '用户登录', '114.247.36.126', '1571966045');
INSERT INTO `cd_log` VALUES ('260', '1', 'admin', '用户登录', '36.57.151.121', '1571966481');
INSERT INTO `cd_log` VALUES ('261', '1', 'admin', '用户登录', '60.169.131.213', '1571966505');
INSERT INTO `cd_log` VALUES ('262', '1', 'admin', '用户登录', '125.71.196.221', '1571967239');
INSERT INTO `cd_log` VALUES ('263', '1', 'admin', '用户登录', '113.110.179.168', '1571967481');
INSERT INTO `cd_log` VALUES ('264', '1', 'admin', '用户登录', '61.184.47.242', '1571967634');
INSERT INTO `cd_log` VALUES ('265', '1', 'admin', '用户登录', '113.250.212.206', '1571968120');
INSERT INTO `cd_log` VALUES ('266', '1', 'admin', '用户登录', '117.136.41.67', '1571968281');
INSERT INTO `cd_log` VALUES ('267', '1', 'admin', '用户登录', '119.139.196.163', '1571968556');
INSERT INTO `cd_log` VALUES ('268', '1', 'admin', '用户登录', '182.90.223.55', '1571969473');
INSERT INTO `cd_log` VALUES ('269', '1', 'admin', '用户登录', '61.157.144.135', '1571969806');
INSERT INTO `cd_log` VALUES ('270', '1', 'admin', '用户登录', '106.120.99.10', '1571970341');
INSERT INTO `cd_log` VALUES ('271', '1', 'admin', '用户登录', '118.122.142.210', '1571970939');
INSERT INTO `cd_log` VALUES ('272', '1', 'admin', '用户登录', '222.185.124.212', '1571971271');
INSERT INTO `cd_log` VALUES ('273', '1', 'admin', '用户登录', '175.43.20.221', '1571971379');
INSERT INTO `cd_log` VALUES ('274', '1', 'admin', '用户登录', '182.138.135.197', '1571971589');
INSERT INTO `cd_log` VALUES ('275', '1', 'admin', '用户登录', '219.145.75.164', '1571971603');
INSERT INTO `cd_log` VALUES ('276', '1', 'admin', '用户登录', '182.138.135.197', '1571971621');
INSERT INTO `cd_log` VALUES ('277', '1', 'admin', '用户登录', '218.17.186.149', '1571972138');
INSERT INTO `cd_log` VALUES ('278', '1', 'admin', '用户登录', '113.116.140.187', '1571973379');
INSERT INTO `cd_log` VALUES ('279', '1', 'admin', '用户登录', '113.118.179.184', '1571973442');
INSERT INTO `cd_log` VALUES ('280', '1', 'admin', '用户登录', '162.14.128.190', '1571975022');
INSERT INTO `cd_log` VALUES ('281', '1', 'admin', '用户登录', '220.202.216.108', '1571975040');
INSERT INTO `cd_log` VALUES ('282', '1', 'admin', '用户登录', '123.149.20.25', '1571975102');
INSERT INTO `cd_log` VALUES ('283', '1', 'admin', '用户登录', '218.7.248.102', '1571975389');
INSERT INTO `cd_log` VALUES ('284', '1', 'admin', '用户登录', '221.205.66.239', '1571976020');
INSERT INTO `cd_log` VALUES ('285', '1', 'admin', '用户登录', '183.39.128.217', '1571977612');
INSERT INTO `cd_log` VALUES ('286', '1', 'admin', '用户登录', '113.109.162.3', '1571977778');
INSERT INTO `cd_log` VALUES ('287', '1', 'admin', '用户登录', '218.7.248.102', '1571978779');
INSERT INTO `cd_log` VALUES ('288', '1', 'admin', '用户登录', '113.140.16.75', '1571979898');
INSERT INTO `cd_log` VALUES ('289', '1', 'admin', '用户登录', '27.186.179.158', '1571980093');
INSERT INTO `cd_log` VALUES ('290', '1', 'admin', '用户登录', '1.196.166.3', '1571980354');
INSERT INTO `cd_log` VALUES ('291', '1', 'admin', '用户登录', '183.160.68.80', '1571980646');
INSERT INTO `cd_log` VALUES ('292', '1', 'admin', '用户登录', '223.167.31.4', '1571982133');
INSERT INTO `cd_log` VALUES ('293', '1', 'admin', '用户登录', '221.12.174.214', '1571982906');
INSERT INTO `cd_log` VALUES ('294', '1', 'admin', '用户登录', '119.131.76.195', '1571982908');
INSERT INTO `cd_log` VALUES ('295', '1', 'admin', '用户登录', '218.81.10.250', '1571983296');
INSERT INTO `cd_log` VALUES ('296', '1', 'admin', '用户登录', '119.28.88.203', '1571984073');
INSERT INTO `cd_log` VALUES ('297', '1', 'admin', '用户登录', '42.226.119.9', '1571984114');
INSERT INTO `cd_log` VALUES ('298', '1', 'admin', '用户登录', '222.82.87.53', '1571985062');
INSERT INTO `cd_log` VALUES ('299', '1', 'admin', '用户登录', '218.7.248.102', '1571986165');
INSERT INTO `cd_log` VALUES ('300', '1', 'admin', '用户登录', '124.114.71.149', '1571986866');
INSERT INTO `cd_log` VALUES ('301', '1', 'admin', '用户登录', '113.108.172.75', '1571988044');
INSERT INTO `cd_log` VALUES ('302', '1', 'admin', '用户登录', '61.157.144.135', '1571988403');
INSERT INTO `cd_log` VALUES ('303', '1', 'admin', '用户登录', '106.120.99.10', '1571989205');
INSERT INTO `cd_log` VALUES ('304', '1', 'admin', '用户登录', '14.153.185.89', '1571990488');
INSERT INTO `cd_log` VALUES ('305', '1', 'admin', '用户登录', '223.83.131.2', '1571990565');
INSERT INTO `cd_log` VALUES ('306', '1', 'admin', '用户登录', '122.230.97.0', '1571992332');
INSERT INTO `cd_log` VALUES ('307', '1', 'admin', '用户登录', '61.157.144.135', '1571992453');
INSERT INTO `cd_log` VALUES ('308', '1', 'admin', '用户登录', '112.198.9.3', '1571993027');
INSERT INTO `cd_log` VALUES ('309', '1', 'admin', '用户登录', '125.71.79.251', '1571993257');
INSERT INTO `cd_log` VALUES ('310', '1', 'admin', '用户登录', '182.245.38.154', '1571993563');
INSERT INTO `cd_log` VALUES ('311', '1', 'admin', '用户登录', '124.237.89.254', '1571993702');
INSERT INTO `cd_log` VALUES ('312', '1', 'admin', '用户登录', '222.85.139.17', '1571993748');
INSERT INTO `cd_log` VALUES ('313', '1', 'admin', '用户登录', '125.127.32.169', '1571994776');
INSERT INTO `cd_log` VALUES ('314', '1', 'admin', '用户登录', '106.125.178.72', '1571995526');
INSERT INTO `cd_log` VALUES ('315', '1', 'admin', '用户登录', '114.94.83.214', '1571995867');
INSERT INTO `cd_log` VALUES ('316', '1', 'admin', '用户登录', '111.199.18.141', '1571996542');
INSERT INTO `cd_log` VALUES ('317', '1', 'admin', '用户登录', '14.153.185.89', '1572001227');
INSERT INTO `cd_log` VALUES ('318', '1', 'admin', '用户登录', '112.31.98.185', '1572003543');
INSERT INTO `cd_log` VALUES ('319', '1', 'admin', '用户登录', '115.52.146.247', '1572007832');
INSERT INTO `cd_log` VALUES ('320', '1', 'admin', '用户登录', '110.188.95.215', '1572007850');
INSERT INTO `cd_log` VALUES ('321', '1', 'admin', '用户登录', '218.88.139.50', '1572008655');
INSERT INTO `cd_log` VALUES ('322', '1', 'admin', '用户登录', '120.40.193.172', '1572015131');
INSERT INTO `cd_log` VALUES ('323', '1', 'admin', '用户登录', '117.136.104.79', '1572015362');
INSERT INTO `cd_log` VALUES ('324', '1', 'admin', '用户登录', '59.57.200.241', '1572016203');
INSERT INTO `cd_log` VALUES ('325', '1', 'admin', '用户登录', '115.51.114.18', '1572016480');
INSERT INTO `cd_log` VALUES ('326', '1', 'admin', '用户登录', '115.53.240.122', '1572016637');
INSERT INTO `cd_log` VALUES ('327', '1', 'admin', '用户登录', '180.97.204.246', '1572016736');
INSERT INTO `cd_log` VALUES ('328', '1', 'admin', '用户登录', '220.202.152.85', '1572016803');
INSERT INTO `cd_log` VALUES ('329', '1', 'admin', '用户登录', '123.15.191.209', '1572017243');
INSERT INTO `cd_log` VALUES ('330', '1', 'admin', '用户登录', '113.15.177.247', '1572017364');
INSERT INTO `cd_log` VALUES ('331', '1', 'admin', '用户登录', '116.27.14.73', '1572017469');
INSERT INTO `cd_log` VALUES ('332', '1', 'admin', '用户登录', '220.202.216.108', '1572017778');
INSERT INTO `cd_log` VALUES ('333', '1', 'admin', '用户登录', '113.69.199.245', '1572018021');
INSERT INTO `cd_log` VALUES ('334', '1', 'admin', '用户登录', '42.236.154.94', '1572018109');
INSERT INTO `cd_log` VALUES ('335', '1', 'admin', '用户登录', '112.10.50.147', '1572018345');
INSERT INTO `cd_log` VALUES ('336', '1', 'admin', '用户登录', '125.76.205.125', '1572018419');
INSERT INTO `cd_log` VALUES ('337', '1', 'admin', '用户登录', '27.18.39.231', '1572018652');
INSERT INTO `cd_log` VALUES ('338', '1', 'admin', '用户登录', '183.17.127.122', '1572019469');
INSERT INTO `cd_log` VALUES ('339', '1', 'admin', '用户登录', '120.29.104.196', '1572020666');
INSERT INTO `cd_log` VALUES ('340', '1', 'admin', '用户登录', '114.243.221.57', '1572021037');
INSERT INTO `cd_log` VALUES ('341', '1', 'admin', '用户登录', '183.0.143.47', '1572021199');
INSERT INTO `cd_log` VALUES ('342', '1', 'admin', '用户登录', '121.239.146.168', '1572021275');
INSERT INTO `cd_log` VALUES ('343', '1', 'admin', '用户登录', '182.204.208.182', '1572021749');
INSERT INTO `cd_log` VALUES ('344', '1', 'admin', '用户登录', '222.209.161.2', '1572022550');
INSERT INTO `cd_log` VALUES ('345', '1', 'admin', '用户登录', '106.114.88.120', '1572024066');
INSERT INTO `cd_log` VALUES ('346', '1', 'admin', '用户登录', '60.1.91.105', '1572024425');
INSERT INTO `cd_log` VALUES ('347', '1', 'admin', '用户登录', '125.76.205.125', '1572024667');
INSERT INTO `cd_log` VALUES ('348', '1', 'admin', '用户登录', '120.29.104.196', '1572024706');
INSERT INTO `cd_log` VALUES ('349', '1', 'admin', '用户登录', '218.85.38.146', '1572024734');
INSERT INTO `cd_log` VALUES ('350', '1', 'admin', '用户登录', '119.131.117.126', '1572024920');
INSERT INTO `cd_log` VALUES ('351', '1', 'admin', '用户登录', '196.61.75.15', '1572026153');
INSERT INTO `cd_log` VALUES ('352', '1', 'admin', '用户登录', '36.161.21.241', '1572027052');
INSERT INTO `cd_log` VALUES ('353', '1', 'admin', '用户登录', '1.196.166.3', '1572027547');
INSERT INTO `cd_log` VALUES ('354', '1', 'admin', '用户登录', '110.19.175.45', '1572029945');
INSERT INTO `cd_log` VALUES ('355', '1', 'admin', '用户登录', '27.11.96.217', '1572036985');
INSERT INTO `cd_log` VALUES ('356', '1', 'admin', '用户登录', '223.104.210.190', '1572038904');
INSERT INTO `cd_log` VALUES ('357', '1', 'admin', '用户登录', '112.12.136.131', '1572041302');
INSERT INTO `cd_log` VALUES ('358', '1', 'admin', '用户登录', '119.123.40.155', '1572046095');
INSERT INTO `cd_log` VALUES ('359', '1', 'admin', '用户登录', '182.107.240.158', '1572046400');
INSERT INTO `cd_log` VALUES ('360', '1', 'admin', '用户登录', '171.223.100.180', '1572047939');
INSERT INTO `cd_log` VALUES ('361', '1', 'admin', '用户登录', '115.234.103.188', '1572049189');
INSERT INTO `cd_log` VALUES ('362', '1', 'admin', '用户登录', '116.252.104.196', '1572049959');
INSERT INTO `cd_log` VALUES ('363', '1', 'admin', '用户登录', '113.88.170.8', '1572052249');
INSERT INTO `cd_log` VALUES ('364', '1', 'admin', '用户登录', '116.252.104.196', '1572052487');
INSERT INTO `cd_log` VALUES ('365', '1', 'admin', '用户登录', '113.88.169.242', '1572053305');
INSERT INTO `cd_log` VALUES ('366', '1', 'admin', '用户登录', '58.58.181.229', '1572054246');
INSERT INTO `cd_log` VALUES ('367', '1', 'admin', '用户登录', '103.225.21.28', '1572054348');
INSERT INTO `cd_log` VALUES ('368', '1', 'admin', '用户登录', '123.232.113.180', '1572054570');
INSERT INTO `cd_log` VALUES ('369', '1', 'admin', '用户登录', '36.7.140.174', '1572054899');
INSERT INTO `cd_log` VALUES ('370', '1', 'admin', '用户登录', '116.27.232.159', '1572056526');
INSERT INTO `cd_log` VALUES ('371', '1', 'admin', '用户登录', '113.88.170.8', '1572056529');
INSERT INTO `cd_log` VALUES ('372', '1', 'admin', '用户登录', '116.252.104.196', '1572056808');
INSERT INTO `cd_log` VALUES ('373', '1', 'admin', '用户登录', '182.202.1.180', '1572057701');
INSERT INTO `cd_log` VALUES ('374', '1', 'admin', '用户登录', '59.42.238.232', '1572058433');
INSERT INTO `cd_log` VALUES ('375', '1', 'admin', '用户登录', '113.137.92.54', '1572059072');
INSERT INTO `cd_log` VALUES ('376', '1', 'admin', '用户登录', '59.174.235.97', '1572059132');
INSERT INTO `cd_log` VALUES ('377', '1', 'admin', '用户登录', '171.15.63.86', '1572059910');
INSERT INTO `cd_log` VALUES ('378', '1', 'admin', '用户登录', '101.83.210.88', '1572059928');
INSERT INTO `cd_log` VALUES ('379', '1', 'admin', '用户登录', '123.116.210.63', '1572059962');
INSERT INTO `cd_log` VALUES ('380', '1', 'admin', '用户登录', '125.73.122.62', '1572060566');
INSERT INTO `cd_log` VALUES ('381', '1', 'admin', '用户登录', '47.52.223.201', '1572060810');
INSERT INTO `cd_log` VALUES ('382', '1', 'admin', '用户登录', '1.198.214.210', '1572061189');
INSERT INTO `cd_log` VALUES ('383', '1', 'admin', '用户登录', '183.30.57.64', '1572061277');
INSERT INTO `cd_log` VALUES ('384', '1', 'admin', '用户登录', '222.172.134.88', '1572062311');
INSERT INTO `cd_log` VALUES ('385', '1', 'admin', '用户登录', '222.209.161.2', '1572062813');
INSERT INTO `cd_log` VALUES ('386', '1', 'admin', '用户登录', '139.215.143.13', '1572063609');
INSERT INTO `cd_log` VALUES ('387', '1', 'admin', '用户登录', '116.252.104.196', '1572063939');
INSERT INTO `cd_log` VALUES ('388', '1', 'admin', '用户登录', '219.136.76.157', '1572065214');
INSERT INTO `cd_log` VALUES ('389', '1', 'admin', '用户登录', '113.128.131.189', '1572067015');
INSERT INTO `cd_log` VALUES ('390', '1', 'admin', '用户登录', '111.112.209.50', '1572067981');
INSERT INTO `cd_log` VALUES ('391', '1', 'admin', '用户登录', '115.60.177.5', '1572069006');
INSERT INTO `cd_log` VALUES ('392', '1', 'admin', '用户登录', '219.136.76.157', '1572069020');
INSERT INTO `cd_log` VALUES ('393', '1', 'admin', '用户登录', '123.233.45.48', '1572069282');
INSERT INTO `cd_log` VALUES ('394', '1', 'admin', '用户登录', '115.60.177.5', '1572069394');
INSERT INTO `cd_log` VALUES ('395', '1', 'admin', '用户登录', '221.225.161.195', '1572069494');
INSERT INTO `cd_log` VALUES ('396', '1', 'admin', '用户登录', '182.138.128.132', '1572070096');
INSERT INTO `cd_log` VALUES ('397', '1', 'admin', '用户登录', '113.88.170.8', '1572071085');
INSERT INTO `cd_log` VALUES ('398', '1', 'admin', '用户登录', '115.60.177.5', '1572071400');
INSERT INTO `cd_log` VALUES ('399', '1', 'admin', '用户登录', '113.66.37.62', '1572071695');
INSERT INTO `cd_log` VALUES ('400', '1', 'admin', '用户登录', '118.76.81.181', '1572073203');
INSERT INTO `cd_log` VALUES ('401', '1', 'admin', '用户登录', '218.77.62.102', '1572073483');
INSERT INTO `cd_log` VALUES ('402', '1', 'admin', '用户登录', '106.114.200.128', '1572073496');
INSERT INTO `cd_log` VALUES ('403', '1', 'admin', '用户登录', '106.114.200.128', '1572073891');
INSERT INTO `cd_log` VALUES ('404', '1', 'admin', '用户登录', '未知IP', '1572074441');
INSERT INTO `cd_log` VALUES ('405', '1', 'admin', '用户登录', '27.18.39.231', '1572075076');
INSERT INTO `cd_log` VALUES ('406', '1', 'admin', '用户登录', '218.88.139.50', '1572075421');
INSERT INTO `cd_log` VALUES ('407', '1', 'admin', '用户登录', '36.63.132.250', '1572075924');
INSERT INTO `cd_log` VALUES ('408', '1', 'admin', '用户登录', '124.164.247.101', '1572076086');
INSERT INTO `cd_log` VALUES ('409', '1', 'admin', '用户登录', '106.6.173.140', '1572076614');
INSERT INTO `cd_log` VALUES ('410', '1', 'admin', '用户登录', '139.215.143.13', '1572076694');
INSERT INTO `cd_log` VALUES ('411', '1', 'admin', '用户登录', '120.198.136.244', '1572078244');
INSERT INTO `cd_log` VALUES ('412', '1', 'admin', '用户登录', '218.89.43.38', '1572078827');
INSERT INTO `cd_log` VALUES ('413', '1', 'admin', '用户登录', '119.98.191.160', '1572078940');
INSERT INTO `cd_log` VALUES ('414', '1', 'admin', '用户登录', '176.122.145.226', '1572078944');
INSERT INTO `cd_log` VALUES ('415', '1', 'admin', '用户登录', '113.128.131.189', '1572079691');
INSERT INTO `cd_log` VALUES ('416', '1', 'admin', '用户登录', '106.114.200.128', '1572080007');
INSERT INTO `cd_log` VALUES ('417', '1', 'admin', '用户登录', '113.66.44.46', '1572080333');
INSERT INTO `cd_log` VALUES ('418', '1', 'admin', '用户登录', '113.71.42.78', '1572080446');
INSERT INTO `cd_log` VALUES ('419', '1', 'admin', '用户登录', '116.252.104.196', '1572080589');
INSERT INTO `cd_log` VALUES ('420', '1', 'admin', '用户登录', '125.64.220.16', '1572080778');
INSERT INTO `cd_log` VALUES ('421', '1', 'admin', '用户登录', '113.87.181.188', '1572083666');
INSERT INTO `cd_log` VALUES ('422', '1', 'admin', '用户登录', '1.80.240.93', '1572084241');
INSERT INTO `cd_log` VALUES ('423', '1', 'admin', '用户登录', '125.76.186.98', '1572084242');
INSERT INTO `cd_log` VALUES ('424', '1', 'admin', '用户登录', '113.117.196.101', '1572085614');
INSERT INTO `cd_log` VALUES ('425', '1', 'admin', '用户登录', '125.76.186.98', '1572088598');
INSERT INTO `cd_log` VALUES ('426', '1', 'admin', '用户登录', '27.154.207.251', '1572089205');
INSERT INTO `cd_log` VALUES ('427', '1', 'admin', '用户登录', '1.198.220.156', '1572089730');
INSERT INTO `cd_log` VALUES ('428', '1', 'admin', '用户登录', '101.70.221.148', '1572091359');
INSERT INTO `cd_log` VALUES ('429', '1', 'admin', '用户登录', '119.103.183.43', '1572093459');
INSERT INTO `cd_log` VALUES ('430', '1', 'admin', '用户登录', '171.104.67.250', '1572094912');
INSERT INTO `cd_log` VALUES ('431', '1', 'admin', '用户登录', '222.78.63.65', '1572095637');
INSERT INTO `cd_log` VALUES ('432', '1', 'admin', '用户登录', '183.228.42.15', '1572096657');
INSERT INTO `cd_log` VALUES ('433', '1', 'admin', '用户登录', '14.27.2.254', '1572096661');
INSERT INTO `cd_log` VALUES ('434', '1', 'admin', '用户登录', '117.181.20.227', '1572096929');
INSERT INTO `cd_log` VALUES ('435', '1', 'admin', '用户登录', '218.72.27.23', '1572097107');
INSERT INTO `cd_log` VALUES ('436', '1', 'admin', '用户登录', '220.177.132.62', '1572097238');
INSERT INTO `cd_log` VALUES ('437', '1', 'admin', '用户登录', '183.228.42.15', '1572097367');
INSERT INTO `cd_log` VALUES ('438', '1', 'admin', '用户登录', '112.22.242.213', '1572097450');
INSERT INTO `cd_log` VALUES ('439', '1', 'admin', '用户登录', '120.230.71.220', '1572097517');
INSERT INTO `cd_log` VALUES ('440', '1', 'admin', '用户登录', '183.228.42.15', '1572098977');
INSERT INTO `cd_log` VALUES ('441', '1', 'admin', '用户登录', '183.213.161.34', '1572099708');
INSERT INTO `cd_log` VALUES ('442', '1', 'admin', '用户登录', '183.12.102.78', '1572100453');
INSERT INTO `cd_log` VALUES ('443', '1', 'admin', '用户登录', '222.83.225.146', '1572101062');
INSERT INTO `cd_log` VALUES ('444', '1', 'admin', '用户登录', '223.156.133.138', '1572101684');
INSERT INTO `cd_log` VALUES ('445', '1', 'admin', '用户登录', '101.88.113.151', '1572101880');
INSERT INTO `cd_log` VALUES ('446', '1', 'admin', '用户登录', '115.148.87.226', '1572104101');
INSERT INTO `cd_log` VALUES ('447', '1', 'admin', '用户登录', '114.100.91.172', '1572105586');
INSERT INTO `cd_log` VALUES ('448', '1', 'admin', '用户登录', '101.85.1.217', '1572106277');
INSERT INTO `cd_log` VALUES ('449', '1', 'admin', '用户登录', '117.181.20.227', '1572108486');
INSERT INTO `cd_log` VALUES ('450', '1', 'admin', '用户登录', '120.229.21.76', '1572111595');
INSERT INTO `cd_log` VALUES ('451', '1', 'admin', '用户登录', '125.76.186.98', '1572117787');
INSERT INTO `cd_log` VALUES ('452', '1', 'admin', '用户登录', '106.114.88.120', '1572118205');
INSERT INTO `cd_log` VALUES ('453', '1', 'admin', '用户登录', '171.210.168.150', '1572119150');
INSERT INTO `cd_log` VALUES ('454', '1', 'admin', '用户登录', '106.114.88.120', '1572120515');
INSERT INTO `cd_log` VALUES ('455', '1', 'admin', '用户登录', '112.64.68.56', '1572130560');
INSERT INTO `cd_log` VALUES ('456', '1', 'admin', '用户登录', '110.187.212.60', '1572131896');
INSERT INTO `cd_log` VALUES ('457', '1', 'admin', '用户登录', '112.50.71.247', '1572133305');
INSERT INTO `cd_log` VALUES ('458', '1', 'admin', '用户登录', '123.159.24.161', '1572135240');
INSERT INTO `cd_log` VALUES ('459', '1', 'admin', '用户登录', '175.169.194.255', '1572137025');
INSERT INTO `cd_log` VALUES ('460', '1', 'admin', '用户登录', '112.50.71.247', '1572137947');
INSERT INTO `cd_log` VALUES ('461', '1', 'admin', '用户登录', '113.88.169.53', '1572138192');
INSERT INTO `cd_log` VALUES ('462', '1', 'admin', '用户登录', '111.2.133.118', '1572138234');
INSERT INTO `cd_log` VALUES ('463', '1', 'admin', '用户登录', '111.2.133.118', '1572138246');
INSERT INTO `cd_log` VALUES ('464', '1', 'admin', '用户登录', '118.144.20.166', '1572142605');
INSERT INTO `cd_log` VALUES ('465', '1', 'admin', '用户登录', '223.88.8.113', '1572143892');
INSERT INTO `cd_log` VALUES ('466', '1', 'admin', '用户登录', '116.136.20.162', '1572143917');
INSERT INTO `cd_log` VALUES ('467', '1', 'admin', '用户登录', '120.36.220.192', '1572145659');
INSERT INTO `cd_log` VALUES ('468', '1', 'admin', '用户登录', '223.104.66.53', '1572148662');
INSERT INTO `cd_log` VALUES ('469', '1', 'admin', '用户登录', '124.114.252.227', '1572149593');
INSERT INTO `cd_log` VALUES ('470', '1', 'admin', '用户登录', '175.100.203.30', '1572151065');
INSERT INTO `cd_log` VALUES ('471', '1', 'admin', '用户登录', '14.20.129.200', '1572151558');
INSERT INTO `cd_log` VALUES ('472', '1', 'admin', '用户登录', '103.117.102.158', '1572155772');
INSERT INTO `cd_log` VALUES ('473', '1', 'admin', '用户登录', '123.9.216.38', '1572155927');
INSERT INTO `cd_log` VALUES ('474', '1', 'admin', '用户登录', '110.188.95.215', '1572156893');
INSERT INTO `cd_log` VALUES ('475', '1', 'admin', '用户登录', '222.180.37.26', '1572157165');
INSERT INTO `cd_log` VALUES ('476', '1', 'admin', '用户登录', '222.208.248.161', '1572157761');
INSERT INTO `cd_log` VALUES ('477', '1', 'admin', '用户登录', '112.48.0.70', '1572158000');
INSERT INTO `cd_log` VALUES ('478', '1', 'admin', '用户登录', '124.114.252.227', '1572158739');
INSERT INTO `cd_log` VALUES ('479', '1', 'admin', '用户登录', '222.180.37.26', '1572158969');
INSERT INTO `cd_log` VALUES ('480', '1', 'admin', '用户登录', '182.107.240.245', '1572159712');
INSERT INTO `cd_log` VALUES ('481', '1', 'admin', '用户登录', '27.40.102.151', '1572160287');
INSERT INTO `cd_log` VALUES ('482', '1', 'admin', '用户登录', '110.87.70.173', '1572163680');
INSERT INTO `cd_log` VALUES ('483', '1', 'admin', '用户登录', '124.114.252.227', '1572165346');
INSERT INTO `cd_log` VALUES ('484', '1', 'admin', '用户登录', '171.117.45.185', '1572166846');
INSERT INTO `cd_log` VALUES ('485', '1', 'admin', '用户登录', '124.114.252.227', '1572167550');
INSERT INTO `cd_log` VALUES ('486', '1', 'admin', '用户登录', '171.117.45.185', '1572167885');
INSERT INTO `cd_log` VALUES ('487', '1', 'admin', '用户登录', '120.36.220.192', '1572170871');
INSERT INTO `cd_log` VALUES ('488', '1', 'admin', '用户登录', '124.114.252.227', '1572172545');
INSERT INTO `cd_log` VALUES ('489', '1', 'admin', '用户登录', '119.131.106.43', '1572173860');
INSERT INTO `cd_log` VALUES ('490', '1', 'admin', '用户登录', '124.114.252.227', '1572176206');
INSERT INTO `cd_log` VALUES ('491', '1', 'admin', '用户登录', '116.136.20.162', '1572176471');
INSERT INTO `cd_log` VALUES ('492', '1', 'admin', '用户登录', '119.28.143.232', '1572177166');
INSERT INTO `cd_log` VALUES ('493', '1', 'admin', '用户登录', '183.200.15.70', '1572178038');
INSERT INTO `cd_log` VALUES ('494', '1', 'admin', '用户登录', '123.123.221.241', '1572178209');
INSERT INTO `cd_log` VALUES ('495', '1', 'admin', '用户登录', '223.150.217.0', '1572178226');
INSERT INTO `cd_log` VALUES ('496', '1', 'admin', '用户登录', '117.85.5.208', '1572178666');
INSERT INTO `cd_log` VALUES ('497', '1', 'admin', '用户登录', '61.186.24.74', '1572179734');
INSERT INTO `cd_log` VALUES ('498', '1', 'admin', '用户登录', '124.114.252.227', '1572181864');
INSERT INTO `cd_log` VALUES ('499', '1', 'admin', '用户登录', '203.177.167.200', '1572182116');
INSERT INTO `cd_log` VALUES ('500', '1', 'admin', '用户登录', '101.70.210.171', '1572182168');
INSERT INTO `cd_log` VALUES ('501', '1', 'admin', '用户登录', '49.80.250.34', '1572182547');
INSERT INTO `cd_log` VALUES ('502', '1', 'admin', '用户登录', '222.209.161.2', '1572182875');
INSERT INTO `cd_log` VALUES ('503', '1', 'admin', '用户登录', '111.19.33.174', '1572183248');
INSERT INTO `cd_log` VALUES ('504', '1', 'admin', '用户登录', '117.90.154.87', '1572183678');
INSERT INTO `cd_log` VALUES ('505', '1', 'admin', '用户登录', '120.204.212.36', '1572184921');
INSERT INTO `cd_log` VALUES ('506', '1', 'admin', '用户登录', '117.149.20.202', '1572185263');
INSERT INTO `cd_log` VALUES ('507', '1', 'admin', '用户登录', '171.214.150.50', '1572186147');
INSERT INTO `cd_log` VALUES ('508', '1', 'admin', '用户登录', '120.231.14.246', '1572186187');
INSERT INTO `cd_log` VALUES ('509', '1', 'admin', '用户登录', '39.67.39.214', '1572186876');
INSERT INTO `cd_log` VALUES ('510', '1', 'admin', '用户登录', '27.38.9.26', '1572187575');
INSERT INTO `cd_log` VALUES ('511', '1', 'admin', '用户登录', '1.80.158.244', '1572188450');
INSERT INTO `cd_log` VALUES ('512', '1', 'admin', '用户登录', '222.209.161.2', '1572189714');
INSERT INTO `cd_log` VALUES ('513', '1', 'admin', '用户登录', '153.0.1.143', '1572189760');
INSERT INTO `cd_log` VALUES ('514', '1', 'admin', '用户登录', '121.235.101.131', '1572190814');
INSERT INTO `cd_log` VALUES ('515', '1', 'admin', '用户登录', '112.9.244.98', '1572191038');
INSERT INTO `cd_log` VALUES ('516', '1', 'admin', '用户登录', '27.18.39.231', '1572191053');
INSERT INTO `cd_log` VALUES ('517', '1', 'admin', '用户登录', '222.209.161.2', '1572191762');
INSERT INTO `cd_log` VALUES ('518', '1', 'admin', '用户登录', '175.5.101.122', '1572192317');
INSERT INTO `cd_log` VALUES ('519', '1', 'admin', '用户登录', '171.214.150.50', '1572192479');
INSERT INTO `cd_log` VALUES ('520', '1', 'admin', '用户登录', '222.209.161.2', '1572194822');
INSERT INTO `cd_log` VALUES ('521', '1', 'admin', '用户登录', '61.241.216.131', '1572194896');
INSERT INTO `cd_log` VALUES ('522', '1', 'admin', '用户登录', '182.150.161.179', '1572195386');
INSERT INTO `cd_log` VALUES ('523', '1', 'admin', '用户登录', '124.130.40.194', '1572196830');
INSERT INTO `cd_log` VALUES ('524', '1', 'admin', '用户登录', '114.225.70.235', '1572196876');
INSERT INTO `cd_log` VALUES ('525', '1', 'admin', '用户登录', '121.231.48.174', '1572196879');
INSERT INTO `cd_log` VALUES ('526', '1', 'admin', '用户登录', '183.3.215.239', '1572203452');
INSERT INTO `cd_log` VALUES ('527', '1', 'admin', '用户登录', '未知IP', '1572208493');
INSERT INTO `cd_log` VALUES ('528', '1', 'admin', '用户登录', '124.114.252.227', '1572216272');
INSERT INTO `cd_log` VALUES ('529', '1', 'admin', '用户登录', '218.66.232.178', '1572223176');
INSERT INTO `cd_log` VALUES ('530', '1', 'admin', '用户登录', '171.215.206.76', '1572223287');
INSERT INTO `cd_log` VALUES ('531', '1', 'admin', '用户登录', '106.6.78.78', '1572224077');
INSERT INTO `cd_log` VALUES ('532', '1', 'admin', '用户登录', '221.7.140.237', '1572224230');
INSERT INTO `cd_log` VALUES ('533', '1', 'admin', '用户登录', '183.42.38.90', '1572224313');
INSERT INTO `cd_log` VALUES ('534', '1', 'admin', '用户登录', '183.42.38.90', '1572224379');
INSERT INTO `cd_log` VALUES ('535', '1', 'admin', '用户登录', '61.52.212.16', '1572224505');
INSERT INTO `cd_log` VALUES ('536', '1', 'admin', '用户登录', '211.196.191.86', '1572224536');
INSERT INTO `cd_log` VALUES ('537', '1', 'admin', '用户登录', '125.78.99.241', '1572225093');
INSERT INTO `cd_log` VALUES ('538', '1', 'admin', '用户登录', '125.73.122.62', '1572225832');
INSERT INTO `cd_log` VALUES ('539', '1', 'admin', '用户登录', '120.198.136.244', '1572226360');
INSERT INTO `cd_log` VALUES ('540', '1', 'admin', '用户登录', '27.212.86.222', '1572226825');
INSERT INTO `cd_log` VALUES ('541', '1', 'admin', '用户登录', '61.141.64.220', '1572226891');
INSERT INTO `cd_log` VALUES ('542', '1', 'admin', '用户登录', '115.202.254.16', '1572228182');
INSERT INTO `cd_log` VALUES ('543', '1', 'admin', '用户登录', '1.196.178.103', '1572228685');
INSERT INTO `cd_log` VALUES ('544', '1', 'admin', '用户登录', '120.197.173.180', '1572228732');
INSERT INTO `cd_log` VALUES ('545', '1', 'admin', '用户登录', '125.73.122.62', '1572230047');
INSERT INTO `cd_log` VALUES ('546', '1', 'admin', '用户登录', '180.159.48.102', '1572230118');
INSERT INTO `cd_log` VALUES ('547', '1', 'admin', '用户登录', '180.159.48.102', '1572230189');
INSERT INTO `cd_log` VALUES ('548', '1', 'admin', '用户登录', '60.27.20.119', '1572231231');
INSERT INTO `cd_log` VALUES ('549', '1', 'admin', '用户登录', '222.78.63.182', '1572231347');
INSERT INTO `cd_log` VALUES ('550', '1', 'admin', '用户登录', '221.232.62.73', '1572231349');
INSERT INTO `cd_log` VALUES ('551', '1', 'admin', '用户登录', '39.129.1.61', '1572231965');
INSERT INTO `cd_log` VALUES ('552', '1', 'admin', '用户登录', '125.73.122.62', '1572232027');
INSERT INTO `cd_log` VALUES ('553', '1', 'admin', '用户登录', '122.224.223.248', '1572233064');
INSERT INTO `cd_log` VALUES ('554', '1', 'admin', '用户登录', '14.134.14.112', '1572234193');
INSERT INTO `cd_log` VALUES ('555', '1', 'admin', '用户登录', '223.74.213.78', '1572234387');
INSERT INTO `cd_log` VALUES ('556', '1', 'admin', '用户登录', '111.160.91.250', '1572236277');
INSERT INTO `cd_log` VALUES ('557', '1', 'admin', '用户登录', '112.9.244.219', '1572236475');
INSERT INTO `cd_log` VALUES ('558', '1', 'admin', '用户登录', '111.225.115.14', '1572238567');
INSERT INTO `cd_log` VALUES ('559', '1', 'admin', '用户登录', '183.223.222.232', '1572238960');
INSERT INTO `cd_log` VALUES ('560', '1', 'admin', '用户登录', '116.22.133.1', '1572239281');
INSERT INTO `cd_log` VALUES ('561', '1', 'admin', '用户登录', '49.84.155.56', '1572239656');
INSERT INTO `cd_log` VALUES ('562', '1', 'admin', '用户登录', '223.72.65.193', '1572240537');
INSERT INTO `cd_log` VALUES ('563', '1', 'admin', '用户登录', '27.189.95.6', '1572240934');
INSERT INTO `cd_log` VALUES ('564', '1', 'admin', '用户登录', '27.18.185.207', '1572241859');
INSERT INTO `cd_log` VALUES ('565', '1', 'admin', '用户登录', '223.72.65.193', '1572242235');
INSERT INTO `cd_log` VALUES ('566', '1', 'admin', '用户登录', '1.27.50.92', '1572242415');
INSERT INTO `cd_log` VALUES ('567', '1', 'admin', '用户登录', '118.122.96.219', '1572242900');
INSERT INTO `cd_log` VALUES ('568', '1', 'admin', '用户登录', '127.0.0.1', '1572249050');
INSERT INTO `cd_log` VALUES ('569', '1', 'admin', '用户登录', '127.0.0.1', '1572266649');
INSERT INTO `cd_log` VALUES ('570', '1', 'admin', '用户登录', '127.0.0.1', '1572314927');
INSERT INTO `cd_log` VALUES ('571', '1', 'admin', '用户登录', '127.0.0.1', '1572324855');
INSERT INTO `cd_log` VALUES ('572', '11', 'test01', '用户登录', '127.0.0.1', '1572325330');
INSERT INTO `cd_log` VALUES ('573', '1', 'admin', '用户登录', '127.0.0.1', '1572325372');
INSERT INTO `cd_log` VALUES ('574', '11', 'test01', '用户登录', '127.0.0.1', '1572325409');
INSERT INTO `cd_log` VALUES ('575', '1', 'admin', '用户登录', '127.0.0.1', '1572325442');
INSERT INTO `cd_log` VALUES ('576', '11', 'test01', '用户登录', '127.0.0.1', '1572325472');
INSERT INTO `cd_log` VALUES ('577', '11', 'test01', '用户登录', '127.0.0.1', '1572325979');
INSERT INTO `cd_log` VALUES ('578', '1', 'admin', '用户登录', '127.0.0.1', '1572326388');
INSERT INTO `cd_log` VALUES ('579', '11', 'test01', '用户登录', '127.0.0.1', '1572326456');
INSERT INTO `cd_log` VALUES ('580', '1', 'admin', '用户登录', '127.0.0.1', '1572326708');
INSERT INTO `cd_log` VALUES ('581', '1', 'admin', '用户登录', '127.0.0.1', '1572328802');
INSERT INTO `cd_log` VALUES ('582', '1', 'admin', '用户登录', '127.0.0.1', '1572333052');

-- ----------------------------
-- Table structure for `cd_member`
-- ----------------------------
DROP TABLE IF EXISTS `cd_member`;
CREATE TABLE `cd_member` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) DEFAULT NULL COMMENT '用户名',
  `sex` tinyint(4) DEFAULT NULL COMMENT '性别',
  `mobile` varchar(250) DEFAULT NULL COMMENT '手机号',
  `email` varchar(250) DEFAULT NULL COMMENT '邮箱',
  `headimg` varchar(250) DEFAULT NULL COMMENT '头像',
  `amount` decimal(10,2) DEFAULT '0.00' COMMENT '积分',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态',
  `province` varchar(250) DEFAULT NULL COMMENT '省地区',
  `city` varchar(250) DEFAULT NULL COMMENT '省地区',
  `district` varchar(250) DEFAULT NULL COMMENT '省地区',
  `password` varchar(250) DEFAULT NULL COMMENT '密码',
  `tags` varchar(250) DEFAULT NULL COMMENT '标签',
  `create_time` int(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_member
-- ----------------------------
INSERT INTO `cd_member` VALUES ('1', 'heyingmin', '1', '13514502847', '274363574@qq.com', '/uploads/admin/201910/5db7d6566e047.jpg', '0.00', '1', '湖北省', '鄂州市', '华容区', '6a5888d05ceb8033ebf0a3dfbf2b416e', 'xhadmin,后台生成,api生成', '1571813109');
INSERT INTO `cd_member` VALUES ('2', '寒塘冷月', '1', '13545028970', '274363574@qq.com', '/uploads/admin/201910/5db7d65025753.jpg', '12.00', '1', '湖北省', '鄂州市', '梁子湖区', '6a5888d05ceb8033ebf0a3dfbf2b416e', '不写一行代码', '1571813181');

-- ----------------------------
-- Table structure for `cd_node`
-- ----------------------------
DROP TABLE IF EXISTS `cd_node`;
CREATE TABLE `cd_node` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `val` varchar(255) NOT NULL,
  `pid` int(4) NOT NULL,
  `sortid` int(4) NOT NULL,
  `status` tinyint(4) DEFAULT '10' COMMENT '状态',
  `is_menu` tinyint(4) DEFAULT NULL,
  `icon` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=265 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_node
-- ----------------------------
INSERT INTO `cd_node` VALUES ('136', '会员管理', '/admin/Member', '0', '1', '1', '1', 'fa fa-asterisk');
INSERT INTO `cd_node` VALUES ('137', '添加', '/admin/Member/add', '136', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('138', '修改', '/admin/Member/update', '138', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('139', '充值', '/admin/Member/recharge', '136', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('140', '回收', '/admin/Member/backRecharge', '136', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('177', '会员列表', '/admin/Member/index', '136', '98', '1', null, null);
INSERT INTO `cd_node` VALUES ('141', '删除', '/admin/Member/delete', '136', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('142', '禁用', '/admin/Member/forbidden', '136', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('143', '启用', '/admin/Member/start', '136', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('144', '重置密码', '/admin/Member/updatePassword', '136', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('145', '批量修改', '/admin/Member/batchUpdate', '136', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('146', '查看数据', '/admin/Member/viewMember', '136', '99', '1', null, null);
INSERT INTO `cd_node` VALUES ('147', 'cms管理', '/admin/Cms', '0', '2', '1', '1', '');
INSERT INTO `cd_node` VALUES ('148', '栏目管理', '/admin/Cms.Catagory', '147', '100', '1', '1', '');
INSERT INTO `cd_node` VALUES ('149', '内容管理', '/admin/Cms.Content', '147', '100', '1', '1', '');
INSERT INTO `cd_node` VALUES ('150', '碎片管理', '/admin/Cms.Frament', '147', '100', '1', '1', '');
INSERT INTO `cd_node` VALUES ('151', '推荐位置管理', '/admin/Cms.Position', '147', '100', '1', '1', '');
INSERT INTO `cd_node` VALUES ('152', '友情链接管理', '/admin/Linkcatagory', '152', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('153', '友情链接管理分类', '/admin/Link.LinkCatagory', '250', '100', '1', '1', '');
INSERT INTO `cd_node` VALUES ('154', '友情连接管理', '/admin/Link.Link', '250', '100', '1', '1', '');
INSERT INTO `cd_node` VALUES ('155', '添加', '/admin/Link.Link/add', '154', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('156', '修改', '/admin/Link.Link/update', '154', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('157', '删除', '/admin/Link.Link/delete', '154', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('158', '添加', '/admin/Link.LinkCatagory/add', '153', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('159', '修改', '/admin/Link.LinkCatagory/update', '153', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('160', '删除', '/admin/Link.LinkCatagory/delete', '153', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('161', '添加', '/admin/Cms.Position/add', '151', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('162', '修改', '/admin/Cms.Position/update', '151', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('163', '删除', '/admin/Cms.Position/delete', '151', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('164', '添加', '/admin/Cms.Frament/add', '150', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('165', '修改', '/admin/Cms.Frament/update', '150', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('166', '删除', '/admin/Cms.Frament/delete', '150', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('167', '添加', '/admin/Cms.Content/add', '149', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('168', '修改', '/admin/Content/update', '168', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('169', '删除', '/admin/Cms.Content/delete', '149', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('170', '修改', '/admin/Cms.Content/update', '149', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('171', '设置排序', '/admin/Cms.Content/updateSort', '149', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('172', '文章移动', '/admin/Cms.Content/move', '149', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('173', '设置推荐位', '/admin/Cms.Content/setPosition', '149', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('174', '删除推荐位', '/admin/Cms.Content/delPosition', '149', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('175', '文章发布草稿', '/admin/Cms.Content/setStatus', '149', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('176', '文章列表', '/admin/Cms.Content/index', '149', '99', '1', '0', '');
INSERT INTO `cd_node` VALUES ('179', '碎片列表', '/admin/Cms.Frament/index', '150', '99', '1', '0', '');
INSERT INTO `cd_node` VALUES ('180', '友情链接管理列表', '/admin/Link.LinkCatagory/index', '153', '99', '1', '0', '');
INSERT INTO `cd_node` VALUES ('181', '友情链接列表', '/admin/Link.Link/index', '154', '99', '1', '0', '');
INSERT INTO `cd_node` VALUES ('182', '设置排序', '/admin/Link.Link/updateSort', '154', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('183', '栏目列表', '/admin/Cms.Catagory/index', '148', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('184', '添加', '/admin/Cms.Catagory/add', '148', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('185', '修改', '/admin/Cms.Catagory/update', '148', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('186', '删除', '/admin/Cms.Catagory/delete', '148', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('187', '设置排序', '/admin/Cms.Catagory/updateSort', '148', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('188', '移动排序', '/admin/Cms.Catagory/setSort', '148', '100', '1', '0', '');
INSERT INTO `cd_node` VALUES ('189', '模型管理', '/admin/Extend', '0', '4', '1', '1', '');
INSERT INTO `cd_node` VALUES ('190', '字段管理', '/admin/Field', '0', '5', '1', null, null);
INSERT INTO `cd_node` VALUES ('191', '模型列表', '/admin/Extend/index', '189', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('192', '添加', '/admin/Extend/add', '189', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('193', '修改', '/admin/Extend/update', '189', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('194', '删除', '/admin/Extend/delete', '189', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('195', '设置排序', '/admin/Extend/updateSort', '189', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('196', '字段列表', '/admin/Field/index', '190', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('197', '添加', '/admin/Field/add', '190', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('198', '修改', '/admin/Field/update', '190', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('199', '删除', '/admin/Field/delete', '190', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('200', '设置排序', '/admin/Field/updateSort', '190', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('201', '上下移动排序', '/admin/Field/setSort', '190', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('213', '系统管理', '/admin/Sys', '0', '100', '1', '1', 'fa fa-cogs');
INSERT INTO `cd_node` VALUES ('214', '用户管理', '/admin/User', '213', '100', '1', '1', 'fa fa-user-secret nav-icon');
INSERT INTO `cd_node` VALUES ('215', '分组管理', '/admin/Group', '213', '100', '1', '1', 'fa fa-user nav-icon');
INSERT INTO `cd_node` VALUES ('216', '操作节点', '/admin/Node', '213', '100', '1', '1', '');
INSERT INTO `cd_node` VALUES ('217', '登录日志', '/admin/Log', '213', '100', '1', '1', 'glyphicon glyphicon-log-in nav-icon');
INSERT INTO `cd_node` VALUES ('218', '系统配置', '/admin/Config', '213', '100', '1', '1', 'glyphicon glyphicon-cog nav-icon');
INSERT INTO `cd_node` VALUES ('219', '修改密码', '/admin/Base/password', '213', '100', '1', '1', 'fa fa-lock nav-icon');
INSERT INTO `cd_node` VALUES ('220', '数据备份', '/admin/Backup', '213', '100', '1', '1', 'fa fa-share nav-icon');
INSERT INTO `cd_node` VALUES ('221', '用户列表', '/admin/User/index', '214', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('222', '添加', '/admin/User/add', '214', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('223', '修改', '/admin/User/update', '214', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('224', '删除', '/admin/User/delete', '214', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('225', '修改密码', '/admin/User/updatePassword', '214', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('226', '分组列表', '/admin/Group/index', '215', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('227', '添加', '/admin/Group/add', '215', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('228', '修改', '/admin/Group/update', '215', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('229', '删除', '/admin/Group/delete', '215', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('230', '禁用', '/admin/Group/forbidden', '215', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('231', '启用', '/admin/Group/start', '215', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('232', '设置权限', '/admin/Base/auth', '215', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('233', '禁用', '/admin/User/forbidden', '214', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('234', '启用', '/admin/User/start', '214', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('235', '节点列表', '/admin/Node/index', '216', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('236', '添加', '/admin/Node/add', '216', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('237', '修改', '/admin/Node/update', '216', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('238', '删除', '/admin/Node/delete', '216', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('239', '日志列表', '/admin/Log/index', '217', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('240', '配置列表', '/admin/Config/index', '218', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('241', '修改密码', '/admin/Base/password', '219', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('242', '备份列表', '/admin/Backup/index', '220', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('243', '新建备份', '/admin/Backup/backupData', '220', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('244', '删除', '/admin/Backup/delete', '220', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('245', '数据列表', '/admin/Back/table', '220', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('246', '下载数据', '/admin/Backup/download', '220', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('247', '删除', '/admin/Log/delete', '217', '100', '1', null, null);
INSERT INTO `cd_node` VALUES ('250', '功能管理', '/admin/Function', '0', '3', '1', '1', '');
INSERT INTO `cd_node` VALUES ('251', '推荐位列表', '/admin/Cms.Position/index', '151', '99', '1', '0', '');
INSERT INTO `cd_node` VALUES ('258', '表单管理', '/admin/FormData', '0', '3', '1', '1', '');
INSERT INTO `cd_node` VALUES ('264', '系统首页', '/admin/Index/main', '213', '1', '1', '1', 'fa fa-home');

-- ----------------------------
-- Table structure for `cd_position`
-- ----------------------------
DROP TABLE IF EXISTS `cd_position`;
CREATE TABLE `cd_position` (
  `position_id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL COMMENT '标题',
  `sortid` int(10) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_position
-- ----------------------------
INSERT INTO `cd_position` VALUES ('1', '推荐', '100');
INSERT INTO `cd_position` VALUES ('2', '置顶', '100');

-- ----------------------------
-- Table structure for `cd_user`
-- ----------------------------
DROP TABLE IF EXISTS `cd_user`;
CREATE TABLE `cd_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(24) DEFAULT NULL COMMENT '姓名',
  `user` varchar(24) DEFAULT NULL COMMENT '登录用户名',
  `pwd` varchar(32) DEFAULT NULL COMMENT '登录密码',
  `group_id` tinyint(4) DEFAULT NULL COMMENT '所属分组ID',
  `type` tinyint(4) DEFAULT NULL COMMENT '账户类型 1超级管理员 2普通管理员',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `status` tinyint(4) DEFAULT NULL COMMENT '10正常 0禁用',
  `create_time` int(10) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cd_user
-- ----------------------------
INSERT INTO `cd_user` VALUES ('1', '寒塘冷月', 'admin', 'be9d66c44816346dd13260680f7edbd6', '1', '1', '超级管理员', '1', '1548558919');
INSERT INTO `cd_user` VALUES ('11', '何英敏', 'test01', 'be9d66c44816346dd13260680f7edbd6', '3', '2', '运营账户', '1', '1568180069');

<?php /*a:2:{s:42:"E:\xhcms2\app\admin\view\member\index.html";i:1572333908;s:47:"E:\xhcms2\app\admin\view\common\_container.html";i:1572333908;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit"/><!-- 让360浏览器默认选择webkit内核 -->

    <!-- 全局css -->
    <!-- 全局css -->
    <link rel="shortcut icon" href="__PUBLIC__/static/favicon.ico">
    <link href="__PUBLIC__/static/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="__PUBLIC__/static/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="__PUBLIC__/static/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="__PUBLIC__/static/css/plugins/validate/bootstrapValidator.min.css" rel="stylesheet">
    <link href="__PUBLIC__/static/css/animate.css" rel="stylesheet">
    <link href="__PUBLIC__/static/css/style.css?v=4.1.0" rel="stylesheet">
     <link rel="stylesheet" href="__PUBLIC__/static/js/plugins/layui/css/layui.css?ver=170803"  media="all">
    
    <link href="__PUBLIC__/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="__PUBLIC__/static/css/plugins/webuploader/webuploader.css" rel="stylesheet">
    <link href="__PUBLIC__/static/css/plugins/ztree/zTreeStyle.css" rel="stylesheet">
    <link href="__PUBLIC__/static/css/plugins/jquery-treegrid/css/jquery.treegrid.css" rel="stylesheet"/>
    <!-- <link href="__PUBLIC__/static/css/plugins/ztree/demo.css" rel="stylesheet"> -->

    <!-- 全局js -->
    <script src="__PUBLIC__/static/js/jquery.min.js?v=2.1.4"></script>
    <script src="__PUBLIC__/static/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="__PUBLIC__/static/js/plugins/ztree/jquery.ztree.all.min.js"></script>
    <script src="__PUBLIC__/static/js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
    <script src="__PUBLIC__/static/js/plugins/validate/bootstrapValidator.min.js"></script>
    <script src="__PUBLIC__/static/js/plugins/validate/zh_CN.js"></script>
    <script src="__PUBLIC__/static/js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>
    <script src="__PUBLIC__/static/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
    <script src="__PUBLIC__/static/js/plugins/jquery-treegrid/js/jquery.treegrid.min.js"></script>
    <script src="__PUBLIC__/static/js/plugins/jquery-treegrid/js/jquery.treegrid.bootstrap3.js"></script>
    <script src="__PUBLIC__/static/js/plugins/jquery-treegrid/extension/jquery.treegrid.extension.js"></script>
    <script src="__PUBLIC__/static/js/plugins/layer/layer.min.js"></script>
    <script src="__PUBLIC__/static/js/plugins/iCheck/icheck.min.js"></script>
    <script src="__PUBLIC__/static/js/plugins/layer/laydate/laydate.js"></script>
    <script src="__PUBLIC__/static/js/plugins/webuploader/webuploader.min.js"></script>
    <script src="__PUBLIC__/static/js/common/ajax-object.js"></script>
    <script src="__PUBLIC__/static/js/common/bootstrap-table-object.js"></script>
    <script src="__PUBLIC__/static/js/common/tree-table-object.js"></script>
    <script src="__PUBLIC__/static/js/common/web-upload-object.js"></script>
    <script src="__PUBLIC__/static/js/common/ztree-object.js"></script>
    <script src="__PUBLIC__/static/js/common/Feng.js"></script>
	<script type="text/javascript" src="__PUBLIC__/static/js/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" src="__PUBLIC__/static/js/ueditor/ueditor.all.min.js"> </script>
	<script type="text/javascript" src="__PUBLIC__/static/js/xheditor/xheditor-1.2.2.min.js"></script>
	<script type="text/javascript" src="__PUBLIC__/static/js/xheditor/xheditor_lang/zh-cn.js"></script>
    <script type="text/javascript">
		<?php
			$domains = config('app.domain_bind');
			$app = app('http')->getName();
			if(count($domains) > 0){			
				if(in_array($app,$domains)){
					$ctxPathUrl = '';
				}else{
					$ctxPathUrl = '/'.$app;
				}
			}else{
				$ctxPathUrl = '/'.$app;
			}
		?>
        Feng.addCtx("<?php echo $ctxPathUrl;?>");
        Feng.sessionTimeoutRegistry();
    </script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
	
<div class="row">
	<div class="col-sm-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>会员管理</h5>
				<button style="float:right; margin-top:-10px;" title="刷新页面" type="button" class="btn btn-default btn-outline" onclick="window.location.reload()" id="">
					<i class="fa fa-refresh"></i>
				</button>
			</div>
			<div class="ibox-content">
				<div class="row row-lg">
					<div class="col-sm-12">
						<div class="row" id="searchGroup">
							<div class="col-sm-2">
								<div class="input-group">
									<div class="input-group-btn">
										<button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">用户名</button>
									</div>
									<input type="text" class="form-control" id="username" placeholder="用户名" />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="input-group">
									<div class="input-group-btn">
										<button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">性别</button>
									</div>
									<select class="form-control" id="sex">
										<option value="">请选择</option>
										<option value="1">男</option>
										<option value="2">女</option>
									</select>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="input-group">
									<div class="input-group-btn">
										<button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">手机号</button>
									</div>
									<input type="text" class="form-control" id="mobile" placeholder="手机号" />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="input-group">
									<div class="input-group-btn">
										<button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">邮箱</button>
									</div>
									<input type="text" class="form-control" id="email" placeholder="邮箱" />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="input-group">
									<div class="input-group-btn">
										<button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">状态</button>
									</div>
									<select class="form-control" id="status">
										<option value="">请选择</option>
										<option value="1">开启</option>
										<option value="0">关闭</option>
									</select>
								</div>
							</div>
							<div id="distpicker5">
								<div class="col-sm-2">
									<div class="input-group">
										<div class="input-group-btn">
											<button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">省</button>
										</div>
										<select lay-ignore id="province" class="form-control" ></select>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="input-group">
										<div class="input-group-btn">
											<button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">市</button>
										</div>
										<select lay-ignore id="city" class="form-control" ></select>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="input-group">
										<div class="input-group-btn">
											<button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">区</button>
										</div>
										<select lay-ignore id="district" class="form-control" ></select>
									</div>
								</div>
							</div>
							<script src="__PUBLIC__/static/js/plugins/shengshiqu/distpicker.data.js"></script>
							<script src="__PUBLIC__/static/js/plugins/shengshiqu/distpicker.js"></script>
							<script src="__PUBLIC__/static/js/plugins/shengshiqu/main.js"></script>
							<div class="col-sm-3">
								<div class="input-group">
									<div class="input-group-btn">
										<button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">创建时间开始</button>
									</div>
									<input type="text" autocomplete="off" placeholder="开始时间" class="form-control layer-date" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" id="create_time_start">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="input-group">
									<div class="input-group-btn">
										<button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">创建时间结束</button>
									</div>
									<input type="text" autocomplete="off" placeholder="结束时间" class="form-control layer-date" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" id="create_time_end">
								</div>
							</div>
							<!-- search end -->
							<div class="col-sm-2">
									<button type="button" class="btn btn-primary " onclick="CodeGoods.search()" id="">
										<i class="fa fa-search"></i>&nbsp;搜索
									</button>
							</div>
						</div>
						<div class="btn-group-sm" id="CodeGoodsTableToolbar" role="group">
						<?php if(in_array('/admin/Member/add',session('admin.nodes')) || session('admin.role') == 1): ?>
						<button type="button" id="add" class="btn btn-primary button-margin" onclick="CodeGoods.add()">
						<i class="fa fa-plus"></i>&nbsp;添加
						</button>
						<?php endif; if(in_array('/admin/Member/update',session('admin.nodes')) || session('admin.role') == 1): ?>
						<button type="button" id="update" class="btn btn-success button-margin" onclick="CodeGoods.update()">
						<i class="fa fa-pencil"></i>&nbsp;修改
						</button>
						<?php endif; if(in_array('/admin/Member/delete',session('admin.nodes')) || session('admin.role') == 1): ?>
						<button type="button" id="delete" class="btn btn-danger button-margin" onclick="CodeGoods.delete()">
						<i class="fa fa-trash"></i>&nbsp;删除
						</button>
						<?php endif; if(in_array('/admin/Member/view',session('admin.nodes')) || session('admin.role') == 1): ?>
						<button type="button" id="view" class="btn btn-info button-margin" onclick="CodeGoods.view()">
						<i class="fa fa-plus"></i>&nbsp;查看数据
						</button>
						<?php endif; if(in_array('/admin/Member/dumpData',session('admin.nodes')) || session('admin.role') == 1): ?>
						<button type="button" id="dumpData" class="btn btn-warning button-margin" onclick="CodeGoods.dumpData()">
						<i class="fa fa-download"></i>&nbsp;导出
						</button>
						<?php endif; if(in_array('/admin/Member/import',session('admin.nodes')) || session('admin.role') == 1): ?>
						<button type="button" id="import" class="btn btn-warning button-margin" onclick="CodeGoods.import()">
						<i class="fa fa-upload"></i>&nbsp;导入
						</button>
						<?php endif; if(in_array('/admin/Member/recharge',session('admin.nodes')) || session('admin.role') == 1): ?>
						<button type="button" id="recharge" class="btn btn-primary button-margin" onclick="CodeGoods.recharge()">
						<i class="fa fa-edit"></i>&nbsp;充值积分
						</button>
						<?php endif; if(in_array('/admin/Member/backRecharge',session('admin.nodes')) || session('admin.role') == 1): ?>
						<button type="button" id="backRecharge" class="btn btn-primary button-margin" onclick="CodeGoods.backRecharge()">
						<i class="fa fa-edit"></i>&nbsp;回收积分
						</button>
						<?php endif; if(in_array('/admin/Member/updatePassword',session('admin.nodes')) || session('admin.role') == 1): ?>
						<button type="button" id="updatePassword" class="btn btn-info button-margin" onclick="CodeGoods.updatePassword()">
						<i class="lock"></i>&nbsp;重置密码
						</button>
						<?php endif; ?>
						</div>
						<table id="CodeGoodsTable" data-mobile-responsive="true" data-click-to-select="true">
							<thead><tr><th data-field="selectItem" data-checkbox="true"></th></tr></thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var CodeGoods = {id: "CodeGoodsTable",seItem: null,table: null,layerIndex: -1};

	CodeGoods.initColumn = function () {
 		return [
 			{field: 'selectItem', checkbox: true},
 			{title: '编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
 			{title: '用户名', field: 'username', visible: true, align: 'center', valign: 'middle'},
 			{title: '性别', field: 'sex', visible: true, align: 'center', valign: 'middle',formatter:CodeGoods.sexFormatter},
 			{title: '手机号', field: 'mobile', visible: true, align: 'center', valign: 'middle'},
 			{title: '邮箱', field: 'email', visible: true, align: 'center', valign: 'middle'},
 			{title: '头像', field: 'headimg', visible: true, align: 'center', valign: 'middle',formatter:CodeGoods.headimgFormatter},
 			{title: '积分', field: 'amount', visible: true, align: 'center', valign: 'middle',formatter:CodeGoods.amountFormatter},
 			{title: '状态', field: 'status', visible: true, align: 'center', valign: 'middle',formatter:CodeGoods.statusFormatter},
 			{title: '省地区', field: 'province|city|district', visible: true, align: 'center', valign: 'middle',formatter:CodeGoods.threeAreaFormatter },
 			{title: '标签', field: 'tags', visible: true, align: 'center', valign: 'middle'},
 			{title: '创建时间', field: 'create_time', visible: true, align: 'center', valign: 'middle',formatter:CodeGoods.create_timeFormatter},
 			{title: '操作', field: 'id', visible: true, align: 'center', valign: 'middle',formatter: 'CodeGoods.buttonFormatter'},
 		];
 	};

	CodeGoods.buttonFormatter = function(value,row,index) {
		if(value){
			var str= '';
			str += '<button type="button" class="btn btn-success btn-xs" title="修改"  onclick="CodeGoods.update('+value+')"><i class="fa fa-pencil"></i>&nbsp;修改</button>&nbsp;'
			str += '<button type="button" class="btn btn-danger btn-xs" title="删除"  onclick="CodeGoods.delete('+value+')"><i class="fa fa-trash"></i>&nbsp;删除</button>&nbsp;'
			return str;
		}
	}

	CodeGoods.sexFormatter = function(value,row,index) {
		if(value !== null){
			var value = value.toString();
			switch(value){
				case '1':
					return '<span class="label label-success">男</span>';
				break;
				case '2':
					return '<span class="label label-warning">女</span>';
				break;
			}
		}
	}

	CodeGoods.headimgFormatter = function(value,row,index) {
		if(value){
			return "<a href=\"javascript:void(0)\" onclick=\"openImg('"+value+"')\"><img height='30' src="+value+"></a>";	
		}
	}

	CodeGoods.amountFormatter = function(value,row,index) {
		if(value){
			return '<span class="label label-info">'+value+'</span>';
		}
	}

	CodeGoods.statusFormatter = function(value,row,index) {
		if(value !== null){
			if(value == 1){
				return '<input class="mui-switch mui-switch-animbg status'+row.id+'" type="checkbox" onclick="CodeGoods.updatestatus('+row.id+',0)" checked>';
			}else{
				return '<input class="mui-switch mui-switch-animbg status'+row.id+'"  type="checkbox" onclick="CodeGoods.updatestatus('+row.id+',1)">';
			}
		}
	}


	CodeGoods.updatestatus = function(pk,value) {
		var ajax = new $ax(Feng.ctxPath + "/Member/updateExt", function (data) {
			if ('00' === data.status) {
			} else {
				Feng.error(data.msg);
				$(".status"+pk).prop("checked",!$(".status"+pk).prop("checked"));
			}
		});
		var val = $(".status"+pk).prop("checked") ? 1 : 0;
		ajax.set('id', pk);
		ajax.set('status', value);
		ajax.start();
	}

	CodeGoods.threeAreaFormatter = function(value,row,index) {
		 var areaStr = '';
		 if(row.province){
		 	areaStr += "-"+row.province;
		 }
		 if(row.city){
		 	areaStr += "-"+row.city;
		 }
		 if(row.district){
		 	areaStr += "-"+row.district;
		 }
		areaStr = areaStr.substr(1);
		return areaStr;
	}

	CodeGoods.create_timeFormatter = function(value,row,index) {
		if(value){
			return formatDateTime(value);	
		}
	}

	CodeGoods.formParams = function() {
		var queryData = {};
		queryData['username'] = $("#username").val();
		queryData['sex'] = $("#sex").val();
		queryData['mobile'] = $("#mobile").val();
		queryData['email'] = $("#email").val();
		queryData['headimg'] = $("#headimg").val();
		queryData['status'] = $("#status").val();
		queryData['province'] = $("#province").val();
		queryData['city'] = $("#city").val();
		queryData['district'] = $("#district").val();
		queryData['create_time_start'] = $("#create_time_start").val();
		queryData['create_time_end'] = $("#create_time_end").val();
		return queryData;
	}

	CodeGoods.check = function () {
		var selected = $('#' + this.id).bootstrapTable('getSelections');
		if(selected.length == 0){
			Feng.info("请先选中表格中的某一记录！");
			return false;
		}else{
			CodeGoods.seItem = selected;
			return true;
		}
	};

	CodeGoods.updateExt = function (value) {
	}


	CodeGoods.add = function (value) {
		var url = location.search;
		var index = layer.open({type: 2,title: '添加',area: ['800px', '100%'],fix: false, maxmin: true,content: Feng.ctxPath + '/Member/add'+url});
		this.layerIndex = index;
	}


	CodeGoods.update = function (value) {
		if(value){
			var index = layer.open({type: 2,title: '修改',area: ['800px', '100%'],fix: false, maxmin: true,content: Feng.ctxPath + '/Member/update?id='+value});
		}else{
			if (this.check()) {
				var idx = '';
				$.each(CodeGoods.seItem, function() {
					idx += ',' + this.id;
				});
				idx = idx.substr(1);
				if(idx.indexOf(",") !== -1){
					Feng.info("请选择单条数据！");
					return false;
				}
				var index = layer.open({type: 2,title: '修改',area: ['800px', '100%'],fix: false, maxmin: true,content: Feng.ctxPath + '/Member/update?id='+idx});
				this.layerIndex = index;
			}
		}
	}


	CodeGoods.delete = function (value) {
		if(value){
			Feng.confirm("是否删除选中项？", function () {
				var ajax = new $ax(Feng.ctxPath + "/Member/delete", function (data) {
					if ('00' === data.status) {
						Feng.success(data.msg);
						CodeGoods.table.refresh();
					} else {
						Feng.error(data.msg);
					}
				});
				ajax.set('ids', value);
				ajax.start();
			});
		}else{
			if (this.check()) {
				var idx = '';
				$.each(CodeGoods.seItem, function() {
					idx += ',' + this.id;
				});
				idx = idx.substr(1);
				Feng.confirm("是否删除选中项？", function () {
					var ajax = new $ax(Feng.ctxPath + "/Member/delete", function (data) {
						if ('00' === data.status) {
							Feng.success(data.msg);
							CodeGoods.table.refresh();
						} else {
							Feng.error(data.msg);
						}
					});
					ajax.set('ids', idx);
					ajax.start();
				});
			}
		}
	}


	CodeGoods.view = function (value) {
		if(value){
			var index = layer.open({type: 2,title: '查看数据',area: ['800px', '100%'],fix: false, maxmin: true,content: Feng.ctxPath + '/Member/view?id='+value});
		}else{
			if (this.check()) {
				var idx = '';
				$.each(CodeGoods.seItem, function() {
					idx += ',' + this.id;
				});
				idx = idx.substr(1);
				var index = layer.open({type: 2,title: '查看数据',area: ['800px', '100%'],fix: false, maxmin: true,content: Feng.ctxPath + '/Member/view?id='+idx});
				this.layerIndex = index;
			}
		}
	}


	CodeGoods.dumpData = function (value) {
		Feng.confirm("是否确定导出记录?", function() {
			var index = layer.msg('正在导出下载，请耐心等待...', {
				time : 3600000,
				icon : 16,
				shade : 0.01
			});
			var idx =[];
			$("li input:checked").each(function(){
				idx.push($(this).attr('data-field'));
			});
			var queryData = CodeGoods.formParams();
			window.location.href = Feng.ctxPath + '/Member/dumpData?action_id=526&' + Feng.parseParam(queryData) + '&' +Feng.parseParam(idx);
			setTimeout(function() {
				layer.close(index)
			}, 1000);
		});
	}


	CodeGoods.import = function (value) {
		var index = layer.open({type: 2,title: '导入',area: ['500px', '300px'],fix: false, maxmin: true,content: Feng.ctxPath + '/Member/import'});
		this.layerIndex = index;
	}


	CodeGoods.recharge = function (value) {
		if(value){
			var index = layer.open({type: 2,title: '数值加',area: ['600px', '350px'],fix: false, maxmin: true,content: Feng.ctxPath + '/Member/recharge?id='+value});
			this.layerIndex = index;
		}else{
			if (this.check()) {
				var idx = '';
				$.each(CodeGoods.seItem, function() {
					idx += ',' + this.id;
				});
				idx = idx.substr(1);
				var index = layer.open({type: 2,title: '数值加',area: ['600px', '350px'],fix: false, maxmin: true,content: Feng.ctxPath + '/Member/recharge?id='+idx});
				this.layerIndex = index;
			}
		}
	}


	CodeGoods.backRecharge = function (value) {
		if(value){
			var index = layer.open({type: 2,title: '数值减',area: ['600px', '350px'],fix: false, maxmin: true,content: Feng.ctxPath + '/Member/backRecharge?id='+value});
			this.layerIndex = index;
		}else{
			if (this.check()) {
				var idx = '';
				$.each(CodeGoods.seItem, function() {
					idx += ',' + this.id;
				});
				idx = idx.substr(1);
				if(idx.indexOf(",") !== -1){
					Feng.info("请选择单条数据！");
					return false;
				}
				var index = layer.open({type: 2,title: '数值减',area: ['600px', '350px'],fix: false, maxmin: true,content: Feng.ctxPath + '/Member/backRecharge?id='+idx});
				this.layerIndex = index;
			}
		}
	}


	CodeGoods.updatePassword = function (value) {
		if(value){
			var index = layer.open({type: 2,title: '修改密码',area: ['600px', '350px'],fix: false, maxmin: true,content: Feng.ctxPath + '/Member/updatePassword?id='+value});
			this.layerIndex = index;
		}else{
			if (this.check()) {
				var idx = '';
				$.each(CodeGoods.seItem, function() {
					idx += ',' + this.id;
				});
				idx = idx.substr(1);
				if(idx.indexOf(",") !== -1){
					Feng.info("请选择单条数据！");
					return false;
				}
				var index = layer.open({type: 2,title: '修改密码',area: ['600px', '350px'],fix: false, maxmin: true,content: Feng.ctxPath + '/Member/updatePassword?id='+idx});
				this.layerIndex = index;
			}
		}
	}


	CodeGoods.search = function() {
		CodeGoods.table.refresh({query : CodeGoods.formParams()});
	};

	$(function() {
		var defaultColunms = CodeGoods.initColumn();
		var url = location.search;
		var table = new BSTable(CodeGoods.id, Feng.ctxPath+"/Member/index"+url,defaultColunms,20);
		table.setPaginationType("server");
		table.setQueryParams(CodeGoods.formParams());
		CodeGoods.table = table.init();
	});
</script>

</div>
<script src="__PUBLIC__/static/js/content.js?v=1.0.0"></script>

</body>
</html>

<?php
// +----------------------------------------------------------------------
// | 应用公共文件
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 
// +----------------------------------------------------------------------


use think\helper\Str;

error_reporting(0);


//多级控制器 获取控制其名称
function getControllerName($controller_name){
	if($controller_name && strpos($controller_name,'/') > 0){
		$controller_name = explode('/',$controller_name)[1];
	}
	return $controller_name;
}

//多级控制器 获取use名称
function getUseName($controller_name){
	if($controller_name && strpos($controller_name,'/') > 0){
		$controller_name = str_replace('/','\\',$controller_name);
	}
	return $controller_name;
}

//多级控制器 获取db命名空间
function getDbName($controller_name){
	if($controller_name && strpos($controller_name,'/') > 0){
		$controller_name = '\\'.explode('/',$controller_name)[0];
	}else{
		$controller_name = '';
	}
	return $controller_name;
}


//多级控制器获取视图名称
function getViewName($controller_name){
	if($controller_name && strpos($controller_name,'/') > 0){
		$arr = explode('/',$controller_name);
		$controller_name = ucfirst($arr[0]).'/'.Str::snake($arr[1]);
	}else{
		$controller_name = Str::snake($controller_name);
	}
	return $controller_name;
}

//多级控制器获取url名称
function getUrlName($controller_name){
	if($controller_name && strpos($controller_name,'/') > 0){
		$controller_name = str_replace('/','.',$controller_name);
	}
	return $controller_name;
}
<?php
/**
 * 删除字段中间件
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */

namespace app\admin\http\middleware;
use think\facade\Db; 

class DeleteField
{
	
    public function handle($request, \Closure $next)
    {	
		$data = $request->param();
		
		$fieldInfo = Db::name('field')->where('id',$data['id'])->find();
		$extendInfo = Db::name('extend')->where('extend_id',$fieldInfo['extend_id'])->find();
		
		try{
			foreach(explode('|',$fieldInfo['field']) as $k=>$v){
				$sql = 'ALTER TABLE '.config('database.connections.mysql.prefix').config('my.create_table_pre').$extendInfo['table_name'].' DROP '.$v;
				Db::execute($sql);
			}		
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}	
		
		return $next($request);	
		
    }
}
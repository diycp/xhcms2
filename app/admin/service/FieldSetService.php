<?php

namespace app\admin\service;

class FieldSetService
{

	//字段属性
	//字段属性
	public static function typeField(){
		
		$list=array(
            1=> array(
                'name'=>'文本框',
                'property'=>1,
                ),
            2=> array(
                'name'=>'下拉框',
                'property'=>3,
                ),
            3=> array(
                'name'=>'单选框',
                'property'=>3,
                ),
            4=> array(
                'name'=>'多选框',
                'property'=>1,
                ),
            6=> array(
                'name'=>'文本域',
                'property'=>4,
                ),
            7=> array(
                'name'=>'日期框',
                'property'=>2,
                ),
            8=> array(
                'name'=>'单图上传',
                'property'=>1,
                ),
			9=> array(
                'name'=>'多图上传',
                'property'=>4,
                ),
			10=> array(
                'name'=>'文件上传',
                'property'=>4,
                ),
            11=> array(
                'name'=>'编辑器(xheditor)',
                'property'=>4,
                ),
			12=> array(
                'name'=>'创建时间(后台录入)',
                'property'=>2,
                ),
			16=> array(
                'name'=>'编辑器(ueditor)',
                'property'=>4,
                ),
			13=> array(
                'name'=>'货币',
                'property'=>5,
                ),
			17=> array(
                'name'=>'省市区三级联动',
                'property'=>1,
                ),
			18=> array(
                'name'=>'颜色选择器',
                'property'=>1,
                ),
			20=> array(
                'name'=>'IP',
                'property'=>1,
                ),
            
        );
        return $list;
	}
	
	
	
	//字段的sql属性
    public static function propertyField()
    {
        $list=array(
            1=> array(
                'name'=>'varchar',
                'maxlen'=>250,
                'decimal'=>0,
                ),
            2=> array(
                'name'=>'int',
                'maxlen'=>10,
                'decimal'=>0,
                ),
			3=> array(
                'name'=>'tinyint',
                'maxlen'=>4,
                'decimal'=>0,
                ),
            4=> array(
                'name'=>'text',
                'maxlen'=>0,
                'decimal'=>0,
                ),
            5 => array(
                'name'=>'decimal',
                'maxlen'=>10,
                'decimal'=>2,
                ),
        );
        return $list;
    }
	
	
	//tab菜单列表
    public static function tabList($menu_id)
    {
        $info = \app\admin\db\Menu::getInfo($menu_id);
		if($info['tab_menu']){
			$list = explode('|',$info['tab_menu']);
		}
		return $list;
    }
	
	
}
